import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../providers/day.dart';

import '../main.dart';

class ScheduleItem extends StatelessWidget {
  final int renderType;

  ScheduleItem(this.renderType);

  // returns minute digits with leadin 0 if less than 10
  String doubleDigit(String minute) {
    if (int.parse(minute) < 10) {
      minute = '0' + minute;
    }
    return minute;
  }

  // individual time element with time and icon
  Widget buildTimeElement(TimeOfDay time, IconData icon) {
    Color timeIsSet = Colors.black;
    if (time == null) timeIsSet = Colors.grey;
    return Container(
      width: 70,
      child: Row(
        children: <Widget>[
          Icon(icon, color: timeIsSet),
          SizedBox(width: 4),
          time != null
              ? Text('${time.hour}:${doubleDigit(time.minute.toString())}'
                  //'${day.estimate.hour.toString()}:${doubleDigit(day.estimate.minute.toString())}',
                  )
              : SizedBox(),
        ],
      ),
    );
  }

// builds a full row with necessary time elements
  Widget buildScheduleRow(Location location, TimeOfDay estimate,
      TimeOfDay arrivedAt, TimeOfDay leftAt) {
    if (location.location == 'Toimistolla' ||
        location.location == 'Etätöissä' ||
        location.location == 'Työmatka') {
      return Container(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Row(
          // align children on the extremities and evenly in between
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          // center row vertically
          crossAxisAlignment: CrossAxisAlignment.center,
          // start row elements from left
          //mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // build each time element, not available for unavailable because it needs 2 times
            buildTimeElement(estimate, Icons.access_time),
            //'${day.estimate.hour.toString()}:${doubleDigit(day.estimate.minute.toString())}',
            buildTimeElement(arrivedAt, Icons.timer),
            //'${day.arrivedAt.hour.toString()}:${doubleDigit(day.arrivedAt.minute.toString())}',
            buildTimeElement(leftAt, Icons.timer_off),
            //'${day.leftAt.hour.toString()}:${doubleDigit(day.leftAt.minute.toString())}',
          ],
        ),
      );
    } else
      return SizedBox();
  }

  Widget buildUnavailableRow(String unavailable) {
    if (renderType == 0 && unavailable.isNotEmpty) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Icon(Icons.highlight_off),
          SizedBox(width: 2),
          Text('Poissa: $unavailable'),
        ],
      );
    } else
      return SizedBox();
  }

  // builds a message row if msg exists, used for other and message types
  Widget buildMessageRow(String msg, IconData msgType) {
    if (msg != "" && msg != null) {
      return Container(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(msgType),
            SizedBox(width: 2),
            Text(msg),
          ],
        ),
      );
    } else
      return SizedBox();
  }

  @override
  Widget build(BuildContext context) {
    final day = Provider.of<Day>(context);
    bool isWeekend = false;

    if (DateTime.parse(day.date).weekday == DateTime.sunday ||
        DateTime.parse(day.date).weekday == DateTime.saturday) {
      isWeekend = true;
    }
    return Container(
      decoration: BoxDecoration(
        color: day.location.color,
        border:
            Border(top: BorderSide(color: Colors.blueGrey[600], width: 0.5)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      child: Column(
        children: <Widget>[
          // date and weekday row
          Container(
            margin: const EdgeInsets.symmetric(vertical: 8),
            color: Colors.transparent,
            child: Row(
              children: <Widget>[
                Icon(Icons.calendar_today),
                SizedBox(width: 6),
                // capitalize according to locale
                Text(
                  toBeginningOfSentenceCase(DateFormat('MMMMEEEEd')
                      .format(DateTime.parse(day.date))
                      .toString()),
                  style: TextStyle(
                      color: isWeekend || day.location.location == "Arkipyhä"
                          ? AppColors.dayHoliday
                          : AppColors.dayInfo),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        day.location.location.toString(),
                        style: TextStyles.dayLocation,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          buildScheduleRow(
              day.location, day.estimate, day.arrivedAt, day.leftAt),
          buildUnavailableRow(day.unavailable),
          renderType == 0 ? buildMessageRow(day.other, Icons.info) : SizedBox(),
          buildMessageRow(day.message, Icons.message),
        ],
      ),
    );
  }
}
