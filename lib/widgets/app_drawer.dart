import 'package:flutter/material.dart';
import '../main.dart';
import '../providers/holidays.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('** APP DRAWER ** :: building app drawer');
    //FirebaseAnalytics().logEvent(name: 'App_Drawer_Open', parameters: null);
    final userName = Provider.of<Auth>(context).displayName;

    return Container(
      width: 220,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            AppBar(
              title: Text('Hei $userName!'),
              automaticallyImplyLeading: false,
            ),
            ListTile(
              leading: Icon(Icons.beach_access),
              title: Text('Lomapäivät'),
              subtitle: Row(
                children: [
                  FutureBuilder(
                      future: Provider.of<Holidays>(context, listen: false)
                          .getHolidays(),
                      builder: (ctx, snapshot) => snapshot.connectionState ==
                              ConnectionState.waiting
                          ? Center(
                              child: SizedBox(
                                  height: 16,
                                  width: 16,
                                  child: CircularProgressIndicator()))
                          : Consumer<Holidays>(
                              builder: (ctx, holidayData, child) => Row(
                                children: [
                                  Text(
                                    '(${holidayData.holidaysTotal}) ',
                                    style: TextStyles.drawerLabelGrey,
                                  ),
                                  Text('${holidayData.holidaysLeft} jäljellä',
                                      style: TextStyles.drawerLabel),
                                ],
                              ),
                            )),
                ],
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Kirjaudu ulos'),
              onTap: () {
                Navigator.of(context).pop();
                Provider.of<Auth>(context, listen: false).logout();
              },
            ),
          ],
        ),
      ),
    );
  }
}
