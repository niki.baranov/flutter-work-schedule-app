import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../providers/day.dart';
import '../screens/edit_schedule_screen.dart';
import '../widgets/schedule_item.dart';

import '../main.dart';

class ScheduleList extends StatelessWidget {
  static const routeName = '/shcedule-list';
  final List<Day> schedule;

  ScheduleList(this.schedule);

  @override
  Widget build(BuildContext context) {
    final scaffold = Scaffold.of(context);
    final _controller = ScrollController();
    print('** SCHEDULE LIST ** :: schedule list rebuilding');
    getWeek(String day) {
      DateTime date = DateTime.parse(day);
      int dayOfYear = int.parse(DateFormat("D").format(DateTime.parse(day)));
      return ((dayOfYear - date.weekday + 10) / 7).floor();
    }

    return Container(
      margin: const EdgeInsets.all(0),
      padding: const EdgeInsets.all(0),
      //color: Colors.grey[100],
      //color: Colors.black,
      child: Scrollbar(
        isAlwaysShown: false,
        child: ListView.builder(
          controller: _controller,
          itemCount: schedule.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
              // for not lists create, for gird and list value
              // create: (c) => schedule[i],
              value: schedule[i],
              // opens day etiting screen
              child: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        color: AppColors.weekMonthSeparator,
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            DateTime.parse(schedule[i].date).day == 1
                                ? Container(
                                    child: Text(
                                      toBeginningOfSentenceCase(
                                              DateFormat('MMMM')
                                                  .format(DateTime.parse(
                                                      schedule[i].date))
                                                  .toString()) +
                                          ' ',
                                      style: TextStyles.weekMonthSeparator,
                                    ),
                                  )
                                : SizedBox(),
                            DateTime.parse(schedule[i].date).weekday ==
                                    DateTime.monday
                                ? Container(
                                    child: Text(
                                        'Viikko ${getWeek(schedule[i].date).toString()}',
                                        style: TextStyles.weekMonthSeparator),
                                  )
                                : SizedBox(),
                          ],
                        ),
                      ),
                      Container(
                          color: schedule[i].location.color,
                          //padding: EdgeInsets.only(right: 10),
                          child: ScheduleItem(0)),
                    ],
                  ),
                  Positioned.fill(
                    top: 0,
                    bottom: 0,
                    child: Material(
                      //color: Colors.black,
                      color: Colors.transparent,
                      child: InkWell(
                        //highlightColor: Colors.orange,
                        //hoverColor: Colors.green,
                        //splashColor: Colors.black26,

                        onTap: schedule[i].location.location != 'Arkipyhä'
                            ? () async {
                                final editResult = await Navigator.of(context)
                                    .pushNamed(EditSchedule.routeName,
                                        arguments: schedule[i].dayId);

                                scaffold
                                  ..removeCurrentSnackBar()
                                  ..showSnackBar(SnackBar(
                                      content: Text(editResult != null
                                          ? '$editResult'
                                          : 'Muutokset hylätty.')));
                              }
                            : () {
                                scaffold
                                  ..removeCurrentSnackBar()
                                  ..showSnackBar(SnackBar(
                                      content:
                                          Text('Arkpyhää ei voi muuttaa.')));
                              },
                      ),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
