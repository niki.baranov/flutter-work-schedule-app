import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/schedule_item.dart';
import '../providers/day.dart';

class FiveDaySchedule extends StatefulWidget {
  final user;
  final List<Day> schedule;

  FiveDaySchedule(this.user, this.schedule);

  @override
  _FiveDayScheduleState createState() => _FiveDayScheduleState();
}

class _FiveDayScheduleState extends State<FiveDaySchedule> {
  final ScrollController _scrollController = ScrollController();
  var _expanded = false;
  @override
  Widget build(BuildContext context) {
    print(
        '** 5 DAY LIST ** :: building schedule for user: ${widget.user.userId}');

    // checks whether user is currently active
    // if current time is greater than arrivedAt time = active
    // if current time is greater than leftAt time = inactive
    // values are already passed via widget.schedule
    // uses the first entry for each user aka today
    // time is converted to double to be comparable
    Color isActive() {
      TimeOfDay arrivedAt = widget.schedule[0].arrivedAt;
      TimeOfDay leftAt = widget.schedule[0].leftAt;
      double currentTime = TimeOfDay.now().hour.toDouble() +
          (TimeOfDay.now().minute.toDouble() / 60);
      double arrivedTime;
      double leftTime;

      // cant convert null time of day to double (hour/minute) so ifs are used
      if (arrivedAt != null) {
        arrivedTime =
            arrivedAt.hour.toDouble() + (arrivedAt.minute.toDouble() / 60);
      } else {
        arrivedTime = -1;
      }

      if (leftAt != null) {
        leftTime = leftAt.hour.toDouble() + (leftAt.minute.toDouble() / 60);
      } else {
        leftTime = -1;
      }

      if (currentTime > leftTime && leftTime != -1) return Colors.black45;
      if ((currentTime >= arrivedTime && arrivedTime != -1) ||
          (currentTime <= leftTime && leftTime != -1)) {
        return Colors.black;
      } else
        return Colors.black45;
    }

    return Card(
      elevation: 3,
      color: widget.schedule[0].location.color,
      margin: const EdgeInsets.only(top: 2, bottom: 8, left: 5, right: 5),
      child: Column(
        children: <Widget>[
          ListTile(
            onTap: () {
              setState(() {
                _expanded = !_expanded;
              });
            },
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  '${widget.user.userName}',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: isActive()),
                ),
                Text(
                  '${widget.schedule[0].location.location}',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: isActive()),
                ),
              ],
            ),
            trailing: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
          ),
          if (_expanded)
            Column(
              children: [
                Container(
                  child: ListView.builder(
                    controller: _scrollController,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: widget.schedule.length,
                    itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
                      value: widget.schedule[i],
                      child: ScheduleItem(1),
                      //itemBuilder: (ctx, i) => Container(child: Text(schedule[i].date)),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: widget
                          .schedule[widget.schedule.length - 1].location.color,
                      borderRadius: const BorderRadius.only(
                          bottomLeft: const Radius.circular(10.0),
                          bottomRight: const Radius.circular(10.0))),
                  height: 5,
                ),
              ],
            ),
        ],
      ),
    );
  }
}
