import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';
import '../screens/edit_notification_screen.dart';
import '../providers/notifications.dart';

import '../main.dart';

class NotificationItem extends StatefulWidget {
  final NotificationMessage notification;
  NotificationItem(this.notification);

  @override
  _NotificationItemState createState() => _NotificationItemState();
}

class _NotificationItemState extends State<NotificationItem> {
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final scaffold = Scaffold.of(context);
    final _userId = Provider.of<Auth>(context).userId;
    final _notificationOwner = widget.notification.ownerId;
    Color _editIcon = AppColors.disabledIconButton;
    Color _deleteIcon = AppColors.disabledIconButton;
    if (_userId == _notificationOwner) {
      _editIcon = AppColors.enabledIconButton;
      _deleteIcon = AppColors.deleteButtonIcon;
    }

    return Container(
      child: Card(
        elevation: 3,
        margin: const EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                          widget.notification.notificationMessage.toString(),
                          style: TextStyles.notificationText),
                    ),
                    Container(
                      height: 40,
                      padding: const EdgeInsets.only(right: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(DateFormat('dd.MM.y')
                              .format(widget.notification.lastUpdate)),
                          IconButton(
                              padding: const EdgeInsets.all(0),
                              icon: Icon(Icons.edit),
                              color: _editIcon,
                              // },
                              onPressed: _userId == _notificationOwner
                                  ? () async {
                                      final editResult =
                                          await Navigator.of(context).pushNamed(
                                              EditNotification.routeName,
                                              arguments: widget
                                                  .notification.notificationId);

                                      scaffold
                                        ..removeCurrentSnackBar()
                                        ..showSnackBar(SnackBar(
                                            content: Text(editResult != null
                                                ? '$editResult'
                                                : 'Ei muutoksia')));
                                    }
                                  : null),
                          if (_isLoading)
                            Container(
                              width: 48,
                              padding: const EdgeInsets.all(0),
                              child: Center(
                                  child: SizedBox(
                                      height: 16,
                                      width: 16,
                                      child: CircularProgressIndicator())),
                            )
                          else
                            IconButton(
                                padding: const EdgeInsets.all(0),
                                icon: Icon(
                                  Icons.delete,
                                ),
                                color: _deleteIcon,
                                onPressed: _userId == _notificationOwner
                                    ? () async {
                                        setState(() {
                                          _isLoading = true;
                                        });
                                        try {
                                          await Provider.of<
                                                      NotificationProvider>(
                                                  context,
                                                  listen: false)
                                              .deleteNotification(widget
                                                  .notification.notificationId);
                                          scaffold
                                            ..removeCurrentSnackBar()
                                            ..showSnackBar(SnackBar(
                                                content: Text(
                                                    'Ilmoitus poistettu onnistuneesti.')));
                                        } catch (error) {
                                          scaffold
                                            ..removeCurrentSnackBar()
                                            ..showSnackBar(SnackBar(
                                                content: Text(
                                                    'Ilmoituksen poistaminen epäonnistui.')));
                                        }
                                        setState(() {
                                          _isLoading = false;
                                        });
                                      }
                                    : null),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
