// handles fetching five day schedule for each user

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../helpers/db_helper.dart';
import '../providers/day.dart';
import '../providers/user.dart';
import '../constants/api.dart' as API;

class FiveDay with ChangeNotifier {
  final String authToken;
  List<Day> _fiveday = [];
  List<User> _users = [];

  List<Day> get fiveday {
    return [..._fiveday];
  }

  List<User> get users {
    return [..._users];
  }

  FiveDay(this.authToken, this._fiveday, this._users);

  List<Day> findFiveNextDays(int _userId) {
    DateTime _fromDate = DateTime.now().subtract(Duration(days: 1));
    DateTime _toDate = DateTime.now().add(Duration(days: 4));

    // get users schedule
    List<Day> userSchedule =
        _fiveday.where((day) => day.userId == _userId).toList();

    // convert userSchedule to only contain 5 days
    List<Day> _fiveDayList = userSchedule
        .where((day) =>
            DateTime.parse(day.date).isAfter(_fromDate) &&
            DateTime.parse(day.date).isBefore(_toDate))
        .toList();

    _fiveDayList.sort();
    return _fiveDayList;
  }

  Future<void> fetchFiveDayFromServer() async {
    final url = API.getFiveDayUrl;

    try {
      print(
          '** 5 DAY ** :: Trying to fetch 5 day schedule for all users from server db');

      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $authToken',
      });
      // save returned data as a map
      final extractedData = json.decode(response.body) as Map<String, dynamic>;

      // stop processing data if null was returned
      if (extractedData == null) {
        return;
      }
      // regexp for pulling date in correct format
      RegExp exp =
          RegExp(r"(0[1-9]|[1-2][0-9]|3[0-1]).(0[1-9]|1[0-2]).([0-9]{4})");

      // go through extractedData based in dayId as key
      // data fetched from server db uses dayId as main key for each entry
      extractedData.forEach((dayId, scheduleData) {
        // shorter expression for extractedData
        var loadedSchedule = scheduleData['schedule'][0];
        int tableId = int.parse(scheduleData['table_id']);
        int userId = users.firstWhere((user) => user.tableId == tableId).userId;
        // parse & format date value from "01.05.2020 - perjantai" string
        String date = exp.stringMatch(loadedSchedule['paivamaara'].toString());
        date =
            '${date.split('.')[2]}-${date.split('.')[1]}-${date.split('.')[0]}';
        // save all data in string values to db
        // parsing is done when data is fetched from local db
        DBHelper.insert('fiveDayList', {
          'dayId': dayId.toString(),
          'userId': userId.toString(),
          'date': date,
          'location': loadedSchedule['sijainti'],
          'estimate': loadedSchedule['saapumisarvio'],
          'arrivedAt': loadedSchedule['saavuin'],
          'leftAt': loadedSchedule['lahdin'],
          'unavailable':
              loadedSchedule['poissa'] != null ? loadedSchedule['poissa'] : "",
          'other':
              loadedSchedule['muuta'] != null ? loadedSchedule['muuta'] : "",
          'message': loadedSchedule['viesti_muille'] != null
              ? loadedSchedule['viesti_muille']
              : "",
          'updatedAt':
              scheduleData['updated'] != null ? scheduleData['updated'] : "",
        });

        // print(dayId.toString());
        // print(userId.toString());
        // print(date.toString());
        // print(loadedSchedule['sijainti']);
        // print(loadedSchedule['saapumisarvio']);
        // print(loadedSchedule['saavuin']);
        // print(loadedSchedule['lahdin']);
        // print(loadedSchedule['poissa']);
        // print(loadedSchedule['muuta']);
        // print(loadedSchedule['viesti_muille']);
        // print('---');
      });

      print(
          '** 5 DAY ** :: Extracted 5 day schedule for ${extractedData.length} days');
      await fetchFiveDayFromLocalDb();
    } catch (error) {
      print('** 5 DAY ** :: Error in 5 day schedule extracting from server db');
      print(error);
      inspect(error);
    }
  }

  Future<void> fetchFiveDayFromLocalDb() async {
    try {
      print('** 5 DAY ** :: Fetching 5 day schedule from local db');
      final scheduleList = await DBHelper.getData('fiveDayList');

      final List<Day> schedule = [];

      scheduleList.forEach((day) => schedule.add(Day(
            dayId: int.parse(day['dayId']),
            userId: int.parse(day['userId']),
            date: day['date'],
            location: Location.values.firstWhere(
                (location) => location.location == day['location'].toString()),
            estimate: (day['estimate'] != "" && day['estimate'] != null)
                ? TimeOfDay(
                    hour: int.parse(day['estimate'].split('.')[0]),
                    minute: int.parse(day['estimate'].split('.')[1]))
                : null,
            arrivedAt: (day['arrivedAt'] != "" && day['arrivedAt'] != null)
                ? TimeOfDay(
                    hour: int.parse(day['arrivedAt'].split('.')[0]),
                    minute: int.parse(day['arrivedAt'].split('.')[1]))
                : null,
            leftAt: (day['leftAt'] != "" && day['leftAt'] != null)
                ? TimeOfDay(
                    hour: int.parse(day['leftAt'].split('.')[0]),
                    minute: int.parse(day['leftAt'].split('.')[1]))
                : null,
            unavailable: day['unavailable'],
            other: day['other'],
            message: day['message'],
            updatedAt: day['updatedAt'],
          )));

      if (schedule.toList().isNotEmpty) {
        _fiveday = schedule.toList();
        print('** 5 DAY ** :: Found ${_fiveday.length} entries in local db');
        notifyListeners();
      } else {
        print('** 5 DAY ** :: local 5 day list is empty, load from server');
        await fetchFiveDayFromServer();
      }
    } catch (error) {
      print('** 5 DAY ** :: Error in 5 day schedule extracting from local db');
      inspect(error);
    }
  }
}
