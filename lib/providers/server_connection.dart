import 'dart:io';
import 'package:flutter/material.dart';

import '../models/http_exception.dart';

class ServerConnection with ChangeNotifier {
  bool _isConnected = false;

  bool get isConnected {
    return _isConnected;
  }

  Future<void> checkConnection() async {
    try {
      final result = await InternetAddress.lookup('http://example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        _isConnected = true;

        notifyListeners();
        return true;
      } else {
        _isConnected = false;

        notifyListeners();
        return false;
      }
    } on SocketException catch (error) {
      print(error);
      throw HttpException(error.toString(), 'Yhteyttä ei voitu muodostaa');
    }
  }
}
