// handles fetching and editing of schedule for logged in user

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../models/http_exception.dart';
import '../providers/day.dart';
import '../helpers/db_helper.dart';
import '../constants/api.dart' as API;
import '../providers/holidays.dart';

class Schedule with ChangeNotifier {
  final String authToken;
  final int userId;
  final int tableId;

  // list with days as arrays for a user
  List<Day> _items = [];

  Schedule(this.authToken, this.userId, this.tableId, this._items);

  // getter for the list
  List<Day> get items {
    return [..._items];
  }

  // add leading zero if minute under 10
  String doubleDigit(String minute) {
    if (int.parse(minute) < 10) {
      minute = '0' + minute;
    }
    return minute;
  }

  // find day by day id
  Day findById(int dayId) {
    return _items.firstWhere((day) => day.dayId == dayId);
  }

  // find day based on date
  Day findByDate(String date) {
    return _items.firstWhere((day) => day.date == date);
  }

  // find and return a list of days based on userId
  List<Day> findByUserId(int userId) {
    // find users schedule
    List<Day> _userSchedule =
        _items.where((day) => day.userId == userId).toList();
    // sort schedule
    _userSchedule.sort();
    return [..._userSchedule];
  }

  List<Day> findByDateRange(
    DateTime _fromDate,
    DateTime _toDate,
  ) {
// get users schedule
    List<Day> _userSchedule =
        _items.where((day) => day.userId == userId).toList();

// Add to list only days between date range
// is after  - 1 day = today
// is before + 1 day = last selected day
    _userSchedule = _userSchedule
        .where((day) =>
            DateTime.parse(day.date)
                .isAfter(_fromDate.subtract(Duration(days: 1))) &&
            DateTime.parse(day.date).isBefore(_toDate.add(Duration(days: 1))))
        .toList();

// sort the list
    _userSchedule.sort();
// return a list of days based on selected dates
    return _userSchedule;
  }

  Future<void> updateDay(
      int dayId, Day newSchedule, String startDate, String endDate) async {
    if (endDate != null) {
      print('** SCHEDULE ** :: updating several days');

      DateTime _rangeStart = DateTime.parse(startDate);
      DateTime _rangeEnd = DateTime.parse(endDate);

      // how many days to update
      // int _daysToAdd =
      //     int.parse(_rangeEnd.difference(_rangeStart).inDays.toString()) + 1;
      // print('days to add: $_daysToAdd');

      // create a list of Days to update, exclude weekends
      List<Day> _datesToUpdate = _items
          .where((day) =>
              DateTime.parse(day.date)
                  .isAfter(_rangeStart.subtract(Duration(days: 1))) &&
              DateTime.parse(day.date)
                  .isBefore(_rangeEnd.add(Duration(days: 1))) &&
              DateTime.parse(day.date).weekday != 6 &&
              DateTime.parse(day.date).weekday != 7 &&
              day.location.location != 'Arkipyhä')
          .toList();

      //print('${_datesToUpdate.length} days will be updated');

      // add to a list of days those days that are not a weekend
      // check how many days to add
      print('** SCHEDULE ** :: updating days: $startDate - $endDate');
      print('** SCHEDULE ** :: ${_datesToUpdate.length} days will be updated');
      _datesToUpdate.forEach((day) async {
        final schedIndex =
            _items.indexWhere((sched) => sched.dayId == day.dayId);
        // save existing day details
        var oldDayData = _items[schedIndex];
        final apiUrl = API.updateScheduleUrl + day.dayId.toString();

        if (schedIndex >= 0) {
          print('Date: ${day.date}, dayId: ${day.dayId}');
          // create correct time values for server and local db = string
          String _estimate = newSchedule.estimate != null
              ? "${newSchedule.estimate.hour.toString()}.${doubleDigit(newSchedule.estimate.minute.toString())}"
              : "";
          String _arrivedAt = newSchedule.arrivedAt != null
              ? "${newSchedule.arrivedAt.hour.toString()}.${doubleDigit(newSchedule.arrivedAt.minute.toString())}"
              : "";
          String _leftAt = newSchedule.leftAt != null
              ? "${newSchedule.leftAt.hour.toString()}.${doubleDigit(newSchedule.leftAt.minute.toString())}"
              : "";

          final response = await http.patch(apiUrl,
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer $authToken',
              },
              body: json.encode({
                "sijainti": newSchedule.location.location.toString(),
                "saapumisarvio": _estimate,
                "saavuin": _arrivedAt,
                "lahdin": _leftAt,
                "poissa": newSchedule.unavailable.toString(),
                "muuta": newSchedule.other.toString(),
                "viesti_muille": newSchedule.message.toString()
              }));

          if (response.statusCode >= 400) {
            print('** SCHEDULE ** :: schedule update failed');
            // if htpp returns error code with status 400 or more
            _items[schedIndex].location = oldDayData.location;
            _items[schedIndex].estimate = oldDayData.estimate;
            _items[schedIndex].arrivedAt = oldDayData.arrivedAt;
            _items[schedIndex].leftAt = oldDayData.leftAt;
            _items[schedIndex].unavailable = oldDayData.unavailable;
            _items[schedIndex].other = oldDayData.other;
            _items[schedIndex].message = oldDayData.message;
            notifyListeners();

            throw HttpException(
              'Virhe: ${response.statusCode}',
              'Päivitys päivälle ${toBeginningOfSentenceCase(DateFormat('MMMMEEEEd').format(DateTime.parse(oldDayData.date)).toString())} epäonnistui!',
            );
          }
          // if update is ok aka code 200 or more update local db list
          if (response.statusCode >= 200 && response.statusCode <= 300) {
            print('** SCHEDULE ** :: updating day: ${day.date} in local db');
            DBHelper.insert('scheduleList', {
              'dayId': day.dayId.toString(),
              'userId': day.userId.toString(),
              'date': day.date.toString(),
              'location': newSchedule.location.location.toString(),
              'estimate': _estimate.toString(),
              'arrivedAt': _arrivedAt.toString(),
              'leftAt': _leftAt.toString(),
              'unavailable': newSchedule.unavailable.toString(),
              'other': newSchedule.other.toString(),
              'message': newSchedule.message.toString(),
              'updatedAt': json.decode(response.body).toString(),
            });
            print('** SCHEDULE ** :: updating day: ${day.date} complete');

            _items[schedIndex].location = newSchedule.location;
            _items[schedIndex].estimate = newSchedule.estimate;
            _items[schedIndex].arrivedAt = newSchedule.arrivedAt;
            _items[schedIndex].leftAt = newSchedule.leftAt;
            _items[schedIndex].unavailable = newSchedule.unavailable;
            _items[schedIndex].other = newSchedule.other;
            _items[schedIndex].message = newSchedule.message;
          }
          notifyListeners();
          // what is saaved to db
          // print('updating day: ${findById(dayId).date}');
          // print('location: ${newSchedule.location.location.toString()}');
          // print('estimate: $_estimate');
          // print('arived: $_arrivedAt');
          // print('left: $_leftAt');
          // print('unavailable: ${newSchedule.unavailable.toString()}');
          // print('other: ${newSchedule.other.toString()}');
          // print('message: ${newSchedule.message.toString()}');
          // print('updated: ${json.decode(response.body)}');

          oldDayData = null;
        } else {
          print('** SCHEDULE ** :: ...');
        }
      });
    }
    // else update a single day
    else {
      final schedIndex = _items.indexWhere((sched) => sched.dayId == dayId);
      // save existing day details
      var oldDayData = _items[schedIndex];

      if (oldDayData.location.location == 'Arkipyhä') {
        print('** SCHEDULE ** :: Ei voi muokata Arkipyhää');
        return;
      } else {
        final apiUrl = API.updateScheduleUrl + dayId.toString();

        if (schedIndex >= 0) {
          // create correct time values for server and local db = string
          String _estimate = newSchedule.estimate != null
              ? "${newSchedule.estimate.hour.toString()}.${doubleDigit(newSchedule.estimate.minute.toString())}"
              : "";
          String _arrivedAt = newSchedule.arrivedAt != null
              ? "${newSchedule.arrivedAt.hour.toString()}.${doubleDigit(newSchedule.arrivedAt.minute.toString())}"
              : "";
          String _leftAt = newSchedule.leftAt != null
              ? "${newSchedule.leftAt.hour.toString()}.${doubleDigit(newSchedule.leftAt.minute.toString())}"
              : "";

          final response = await http.patch(apiUrl,
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer $authToken',
              },
              body: json.encode({
                "sijainti": newSchedule.location.location.toString(),
                "saapumisarvio": _estimate,
                "saavuin": _arrivedAt,
                "lahdin": _leftAt,
                "poissa": newSchedule.unavailable.toString(),
                "muuta": newSchedule.other.toString(),
                "viesti_muille": newSchedule.message.toString()
              }));
          if (response.statusCode >= 400) {
            print(
                '** SCHEDULE ** :: update for day ${oldDayData.date.toString()} failed');
            // if htpp returns error code with status 400 or more
            _items[schedIndex].location = oldDayData.location;
            _items[schedIndex].estimate = oldDayData.estimate;
            _items[schedIndex].arrivedAt = oldDayData.arrivedAt;
            _items[schedIndex].leftAt = oldDayData.leftAt;
            _items[schedIndex].unavailable = oldDayData.unavailable;
            _items[schedIndex].other = oldDayData.other;
            _items[schedIndex].message = oldDayData.message;
            notifyListeners();

            throw HttpException(
              'Virhe: ${response.statusCode}',
              'Päivitys päivälle ${toBeginningOfSentenceCase(DateFormat('MMMMEEEEd').format(DateTime.parse(oldDayData.date)).toString())} epäonnistui!',
            );
          }
          // if update is ok aka code 200 or more update local db list
          if (response.statusCode >= 200) {
            DBHelper.insert('scheduleList', {
              'dayId': dayId.toString(),
              'userId': userId.toString(),
              'date': newSchedule.date,
              'location': newSchedule.location.location.toString(),
              'estimate': _estimate.toString(),
              'arrivedAt': _arrivedAt.toString(),
              'leftAt': _leftAt.toString(),
              'unavailable': newSchedule.unavailable.toString(),
              'other': newSchedule.other.toString(),
              'message': newSchedule.message.toString(),
              'updatedAt': json.decode(response.body).toString(),
            });
            // update data locally, if server update is ok
            _items[schedIndex].location = newSchedule.location;
            _items[schedIndex].estimate = newSchedule.estimate;
            _items[schedIndex].arrivedAt = newSchedule.arrivedAt;
            _items[schedIndex].leftAt = newSchedule.leftAt;
            _items[schedIndex].unavailable = newSchedule.unavailable;
            _items[schedIndex].other = newSchedule.other;
            _items[schedIndex].message = newSchedule.message;
            _items[schedIndex].updatedAt = json.decode(response.body);
            print(
                '** SCHEDULE ** :: update for day ${oldDayData.date.toString()} complete');
          }
          // what is saaved to db
          // print('updating day: ${findById(dayId).date}');
          // print('location: ${newSchedule.location.location.toString()}');
          // print('estimate: $_estimate');
          // print('arived: $_arrivedAt');
          // print('left: $_leftAt');
          // print('unavailable: ${newSchedule.unavailable.toString()}');
          // print('other: ${newSchedule.other.toString()}');
          // print('message: ${newSchedule.message.toString()}');
          // print('updated: ${json.decode(response.body)}');

          notifyListeners();
          oldDayData = null;
        } else {
          print('...');
        }
      }
    }
  }

// fetch full schedule for a selected user based on table id
// saves schedule in db as string
// tableId is found by
  Future<void> fetchServerSchedule() async {
    String _tableId = tableId.toString();
    final url = API.getScheduleUrl + tableId.toString();

    try {
      print(
          '** SCHEDULE ** :: fetching schedule from server for user: $userId from tableId: ${_tableId.toString()}');

      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $authToken',
      });
      // save returned data as a map
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      print('** SCHEDULE ** :: Found ${extractedData.length} entries');
      // stop processing data if null was returned
      if (extractedData == null) {
        return;
      }

      // regexp for pulling date in correct format
      RegExp exp =
          RegExp(r"(0[1-9]|[1-2][0-9]|3[0-1]).(0[1-9]|1[0-2]).([0-9]{4})");

      // go through extractedData based in dayId as key
      // data fetched from server db uses dayId as main key for each entry
      extractedData.forEach((dayId, scheduleData) {
        // shorter expression for extractedData
        var loadedSchedule = scheduleData['schedule'][0];

        // INFO
        // userID and tableID are passed to the function directly

        // internal db key : loadeddata
        // 'date': loadedSchedule['paivamaara'],
        // 'location': loadedSchedule['sijainti'],
        // 'estimate': loadedSchedule['saapumisarvio'],
        // 'arrivedAt': loadedSchedule['saavuin'],
        // 'leftAt': loadedSchedule['lahdin'],
        // 'unavailable': loadedSchedule['poissa'],
        // 'other': loadedSchedule['muuta'],
        // 'message': loadedSchedule['viesti_muille'],
        // updated is stored on a different level than schedule info
        // 'updatedAt': scheduleData['updated'],
        // times are saved as string in db

        // tableId value NOT NEEDED
        // int loadedTableId = scheduleData['table_id'];

        // parse updatedAt value NOT NEEDED
        //String updatedAt = DateTime.parse(scheduleData['updated']).toString();

        // location, get value from Location enum NOT NEEDED
        // var loadedLocation = Location.values.firstWhere((location) =>
        //     location.location == loadedSchedule['sijainti'].toString());

        // parse & format date value from "01.05.2020 - perjantai" string
        String date = exp.stringMatch(loadedSchedule['paivamaara'].toString());
        date =
            '${date.split('.')[2]}-${date.split('.')[1]}-${date.split('.')[0]}';

        // save all data in string values to db
        // parsing is done when data is fetched from local db
        DBHelper.insert('scheduleList', {
          'dayId': dayId.toString(),
          'userId': userId.toString(),
          'date': date,
          'location': loadedSchedule['sijainti'],
          'estimate': loadedSchedule['saapumisarvio'],
          'arrivedAt': loadedSchedule['saavuin'],
          'leftAt': loadedSchedule['lahdin'],
          'unavailable':
              loadedSchedule['poissa'] != null ? loadedSchedule['poissa'] : "",
          'other':
              loadedSchedule['muuta'] != null ? loadedSchedule['muuta'] : "",
          'message': loadedSchedule['viesti_muille'] != null
              ? loadedSchedule['viesti_muille']
              : "",
          'updatedAt':
              scheduleData['updated'] != null ? scheduleData['updated'] : "",
        });
      });
      print(
          '** SCHEDULE ** :: Extracted schedule for ${extractedData.length} days');
      await fetchLocalSchedule();
    } catch (error) {
      print('** SCHEDULE ** :: Error in schedule extracting from server db');
      inspect(error);
      throw error;
    }
  }

// fetch schedule list from local db, populate items list
  Future<void> fetchLocalSchedule() async {
    try {
      print('** SCHEDULE ** :: Fetching schedule from local db');
      final scheduleList = await DBHelper.getData('scheduleList');
      final List<Day> schedule = [];

      scheduleList.forEach((day) => schedule.add(Day(
            dayId: int.parse(day['dayId']),
            userId: int.parse(day['userId']),
            date: day['date'],
            location: Location.values.firstWhere(
                (location) => location.location == day['location'].toString()),
            estimate: (day['estimate'] != "" && day['estimate'] != null)
                ? TimeOfDay(
                    hour: int.parse(day['estimate'].split('.')[0]),
                    minute: int.parse(day['estimate'].split('.')[1]))
                : null,
            arrivedAt: (day['arrivedAt'] != "" && day['arrivedAt'] != null)
                ? TimeOfDay(
                    hour: int.parse(day['arrivedAt'].split('.')[0]),
                    minute: int.parse(day['arrivedAt'].split('.')[1]))
                : null,
            leftAt: (day['leftAt'] != "" && day['leftAt'] != null)
                ? TimeOfDay(
                    hour: int.parse(day['leftAt'].split('.')[0]),
                    minute: int.parse(day['leftAt'].split('.')[1]))
                : null,
            unavailable: day['unavailable'],
            other: day['other'],
            message: day['message'],
            updatedAt: day['updatedAt'],
          )));

      if (schedule.toList().isNotEmpty) {
        _items = schedule.toList();
        notifyListeners();
      } else {
        // fetch from server if local db list turns out empty
        print('** SCHEDULE ** :: local schedule is empty, load from server');
        await fetchServerSchedule();
      }
    } catch (error) {
      print('** SCHEDULE ** :: Error in schedule extracting from local db');
      inspect(error);
      throw error;
    }
  }

  Future<void> updateTime(String type) async {
    // set currentDay
    String currentDate =
        DateFormat('y-MM-dd').format(DateTime.now()).toString();

    // get current day from day list
    Day currentDay = findByDate(currentDate);
    final schedIndex =
        _items.indexWhere((day) => day.dayId == currentDay.dayId);
    final apiUrl = API.updateScheduleTimeUrl + currentDay.dayId.toString();

    // check which value to update
    // attempt update on server first, then to local db and local list
    try {
      print('** SCHEUDLE ** :: updating $type for ${currentDay.toString()}');
      TimeOfDay currentTime = TimeOfDay.now();
      String currentTimeString =
          '${currentTime.hour.toString()}.${doubleDigit(currentTime.minute.toString())}';
      // if
      if (type == 'arrivedAt') {
        final response = await http.patch(apiUrl,
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': 'Bearer $authToken',
            },
            body: json.encode({
              "saavuin": currentTimeString,
            }));
        if (response.statusCode >= 200) {
          print('** SCHEUDLE ** :: ${json.decode(response.body).toString()}');
          // update receives name of the table, map of data to update,
          // name of the id column and its value
          DBHelper.update(
            'scheduleList',
            {
              'arrivedAt': currentTimeString,
              'updatedAt': json.decode(response.body).toString(),
            },
            'dayId',
            currentDay.dayId.toString(),
          );
          // update data locally, if server update is ok
          _items[schedIndex].arrivedAt = currentTime;
          _items[schedIndex].updatedAt = json.decode(response.body).toString();
          //notifyListeners();
        }
      }
      if (type == 'leftAt') {
        final response = await http.patch(apiUrl,
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': 'Bearer $authToken',
            },
            body: json.encode({
              "lahdin": currentTimeString,
            }));
        if (response.statusCode >= 200) {
          DBHelper.update(
            'scheduleList',
            {
              'leftAt': currentTimeString,
              'updatedAt': json.decode(response.body).toString(),
            },
            'dayId',
            currentDay.dayId.toString(),
          );
          // update data locally, if server update is ok
          _items[schedIndex].leftAt = currentTime;
          _items[schedIndex].updatedAt = json.decode(response.body).toString();
          //notifyListeners();
        }
      }
      notifyListeners();
    } catch (error) {
      print(error);
      inspect(error);
      throw error;
    }
  }
}
