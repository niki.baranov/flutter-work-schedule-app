// Sets contents of each day
// contains Location enum that provides correct color and string for location

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum Location {
  Toimistolla,
  Remote,
  WorkTrip,
  YearlyHoliday,
  OvertimeHoliday,
  ParentLeave,
  Sick,
  ChildSick,
  UnpayedLeave,
  OtherLeave,
  NonWorkDay,
  Unmarked,
  Holiday,
}

extension LocationExtension on Location {
  // set string values and colors for Location enum values
  static const locations = {
    Location.Toimistolla: 'Toimistolla',
    Location.Remote: 'Etätöissä',
    Location.WorkTrip: 'Työmatka',
    Location.YearlyHoliday: 'Vuosiloma',
    Location.OvertimeHoliday: 'Ylityövapaa',
    Location.ParentLeave: 'Vanhempainvapaa',
    Location.Sick: 'Sairas',
    Location.ChildSick: 'Lapsen sairaus',
    Location.UnpayedLeave: 'Palkaton vapaa',
    Location.OtherLeave: 'Muu poissaolo',
    Location.NonWorkDay: 'Ei työpäivä',
    Location.Unmarked: 'Merkitsemättä',
    Location.Holiday: 'Arkipyhä',
  };

  static const colors = {
    Location.Toimistolla: Color.fromRGBO(119, 255, 122, 1),
    Location.Remote: Color.fromRGBO(255, 231, 56, 1),
    Location.WorkTrip: Color.fromRGBO(255, 231, 56, 1),
    Location.YearlyHoliday: Color.fromRGBO(138, 239, 255, 1),
    Location.OvertimeHoliday: Color.fromRGBO(175, 204, 225, 1),
    Location.ParentLeave: Color.fromRGBO(138, 239, 255, 1),
    Location.Sick: Color.fromRGBO(175, 204, 225, 1),
    Location.ChildSick: Colors.white,
    Location.UnpayedLeave: Color.fromRGBO(175, 204, 225, 1),
    Location.OtherLeave: Color.fromRGBO(175, 204, 225, 1),
    Location.NonWorkDay: Color.fromRGBO(175, 204, 225, 1),
    Location.Unmarked: Colors.white,
    Location.Holiday: Color.fromRGBO(138, 239, 255, 1),
  };

  // return location string
  String get location => locations[this];
  // return location color
  Color get color => colors[this];
}

class Day with ChangeNotifier implements Comparable<Day> {
  final int dayId;
  final int userId;
  final String date;
  Location location;
  TimeOfDay estimate;
  TimeOfDay arrivedAt;
  TimeOfDay leftAt;
  String unavailable;
  String other;
  String message;
  String updatedAt;

  Day({
    @required this.dayId,
    @required this.userId,
    //@required this.tableId,
    @required this.date,
    @required this.location,
    @required this.estimate,
    @required this.arrivedAt,
    @required this.leftAt,
    @required this.unavailable,
    @required this.other,
    @required this.message,
    @required this.updatedAt,
  });

  int compareTo(Day otherDay) {
    int order = DateTime.parse(date).compareTo(DateTime.parse(otherDay.date));
    return order;
  }
}

class EditedDay {
  final int dayId;
  final int userId;
  final String date;
  Location location;
  TimeOfDay estimate;
  TimeOfDay arrivedAt;
  TimeOfDay leftAt;
  String unavailable;
  String other;
  String message;
  String updatedAt;

  EditedDay({
    @required this.dayId,
    @required this.userId,
    @required this.date,
    @required this.location,
    @required this.estimate,
    @required this.arrivedAt,
    @required this.leftAt,
    @required this.unavailable,
    @required this.other,
    @required this.message,
    @required this.updatedAt,
  });
}
