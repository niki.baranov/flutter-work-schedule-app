// handles fetching and setting notifications

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../helpers/db_helper.dart';
import '../models/http_exception.dart';
import '../constants/api.dart' as API;

class NotificationMessage with ChangeNotifier {
  final int notificationId;
  final int ownerId;
  String notificationMessage;
  DateTime lastUpdate;

  NotificationMessage({
    @required this.notificationId,
    @required this.ownerId,
    @required this.notificationMessage,
    @required this.lastUpdate,
  });
}

class NotificationProvider with ChangeNotifier {
  final String authToken;
  final int userId;
  List<NotificationMessage> _notifications = [];

  NotificationProvider(this.authToken, this.userId, this._notifications);

  List<NotificationMessage> get notifications {
    return [..._notifications];
  }

  // find day by day id
  NotificationMessage findById(int notificationId) {
    return _notifications.firstWhere(
        (notification) => notification.notificationId == notificationId);
  }

  Future<void> updateNotification(
      int notificationId, NotificationMessage newNotificationMessage) async {
    print(
        '** NOTIFICATION ** :: updating notification with id $notificationId');
    // find index for notification being edited
    final notificationIndex = _notifications.indexWhere(
        (notification) => notification.notificationId == notificationId);
    // save old notification data in case update to db fails
    var oldNotification = _notifications[notificationIndex];
    // run update if index was found aka notification exists
    if (notificationIndex >= 0) {
      final url = API.updateNotificationUrl + notificationId.toString();
      try {
        final response = await http.patch(url,
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': 'Bearer $authToken',
            },
            body: json.encode({
              "viesti": newNotificationMessage.notificationMessage,
              "owner_id": userId.toString(),
            }));
        // if api access failed
        if (response.statusCode >= 400) {
          _notifications[notificationIndex].notificationMessage =
              oldNotification.notificationMessage;

          throw HttpException(
            'Virhe: ${response.statusCode}',
            'Ilmoituksen päivitys epäonnistui!',
          );
        }
        // if update succeeded update local values
        if (response.statusCode >= 200) {
          DBHelper.update(
            'notificationsList',
            {
              'ownerId': newNotificationMessage.ownerId,
              'notificationMessage': newNotificationMessage.notificationMessage,
              'lastUpdate': json.decode(response.body),
            },
            'notificationId',
            notificationId.toString(),
          );

          _notifications[notificationIndex].notificationMessage =
              newNotificationMessage.notificationMessage;
          _notifications[notificationIndex].lastUpdate =
              DateTime.parse(json.decode(response.body));
          print(
              '** NOTIFICATION ** :: successfully updated notification with id $notificationId');
        }
        notifyListeners();
        oldNotification = null;
      } catch (error) {
        print('** NOTIFICATION ** :: Error saving notification to server db');
        inspect(error);
      }
    }
  }

  Future<void> addNotification(
      NotificationMessage newNotificationMessage) async {
    print('** NOTIFICATION ** :: adding new notification');
    final url = API.createNotificationUrl;

    try {
      final response = await http.post(url,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer $authToken',
          },
          body: json.encode({
            "viesti": newNotificationMessage.notificationMessage,
            "owner_id": userId.toString(),
          }));
      // if api access failed
      if (response.statusCode >= 400) {
        throw HttpException(
          'Virhe: ${response.statusCode}',
          'Ilmoituksen lisäys epäonnistui!',
        );
      }
      // if update succeeded get new list from server
      if (response.statusCode >= 200) {
        await fetchNotificationsFromServer();
        //notifyListeners();
        print('** NOTIFICATION ** :: new notification added to server');
      }
      //return userList;
    } catch (error) {
      print('** NOTIFICATION ** :: Error saving new notification to server db');
      inspect(error);
    }
  }

  Future<void> deleteNotification(int notificationId) async {
    print(
        '** NOTIFICATION ** :: deleting notification with id $notificationId');
    final existingNotificationIndex = _notifications.indexWhere(
        (notification) => notification.notificationId == notificationId);
    print(existingNotificationIndex);
    final url = API.deleteNotificationUrl + notificationId.toString();
    print(url);
    try {
      // post is used instead of delete to allow sending body to check for notification ownerid
      final response = await http.post(url,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer $authToken',
          },
          body: json.encode({
            "owner_id": userId.toString(),
          }));
      print(json.decode(response.statusCode.toString()));

      if (response.statusCode >= 400) {
        // delete from local in case user found a ghost notification

        // remove notification from local list
        _notifications.removeAt(existingNotificationIndex);
        // remove notification from local db
        DBHelper.delete(
          'notificationsList',
          'notificationId',
          notificationId.toString(),
        );

        await fetchNotificationsFromServer();
        throw HttpException(
          'Virhe: ${response.statusCode}',
          'Ilmoituksen poisto palvelimelta epäonnistui!',
        );
      }

      if (response.statusCode >= 200 && response.body == "1") {
        // remove notification from local list
        _notifications.removeAt(existingNotificationIndex);
        // remove notification from local db
        DBHelper.delete(
          'notificationsList',
          'notificationId',
          notificationId.toString(),
        );
        print(
            '** NOTIFICATION ** :: notification with id $notificationId removed');
      }
      await fetchNotificationsFromServer();
    } catch (error) {
      print('** NOTIFICATION ** :: Error deleting notification from server');
      inspect(error);
      throw error;
    }
    notifyListeners();
  }

  Future<void> fetchNotificationsFromServer() async {
    final url = API.getNotificationsUrl;

    try {
      print('** NOTIFICATION ** :: Fetching notifications from server db');
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $authToken',
      });
      final extractedData = json.decode(response.body) as Map<String, dynamic>;

      extractedData.forEach((notificationId, notificationData) {
        DBHelper.insert('notificationsList', {
          'notificationId': notificationId,
          'ownerId': notificationData['owner_id'],
          'notificationMessage': notificationData['viesti'],
          'lastUpdate': notificationData['updated'],
        });
      });
      await fetchNotificationsFromLocalDb();
    } catch (error) {
      print(
          '** NOTIFICATION ** :: Error in notification extraction from server');
      inspect(error);
    }
  }

  Future<void> fetchNotificationsFromLocalDb() async {
    try {
      print('** NOTIFICATION ** :: Fetching notifications from local db');
      final notifications = await DBHelper.getData('notificationsList');

      final List<NotificationMessage> notificationsList = [];

      notifications
          .forEach((notification) => notificationsList.add(NotificationMessage(
                notificationId: int.parse(notification['notificationId']),
                ownerId: int.parse(notification['ownerId']),
                notificationMessage: notification['notificationMessage'],
                lastUpdate: DateTime.parse(notification['lastUpdate']),
              )));

      if (notificationsList.toList().isNotEmpty) {
        _notifications = notificationsList.toList();
        print(
            '** NOTIFICATION ** :: Found ${_notifications.length} notifications in local db');
        notifyListeners();
      } else {
        print(
            '** NOTIFICATION ** :: local notification list is empty, load from server');
        await fetchNotificationsFromServer();
      }
    } catch (error) {
      print(
          '** NOTIFICATION ** :: Error in notifications extraction from local db');
      inspect(error);
    }
  }
}
