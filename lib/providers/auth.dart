import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/http_exception.dart';
import '../constants/api.dart' as API;

class Auth with ChangeNotifier {
  String _token;
  String _data;
  int _userId;
  String _displayName;
  String _email;
  int _tableId;

  bool get isAuth {
    return token != null;
  }

  String get token {
    return _token;
  }

  int get userId {
    return _userId;
  }

  String get displayName {
    return _displayName;
  }

  String get email {
    return _email;
  }

  int get tableId {
    return _tableId;
  }

  Future<void> _authenticate(String email, String password) async {
    // url to api to get token
    final apiUrl = API.getAuthUrl;

    try {
      // wp api jwt auth resoponce
      final response = await http.post(
        apiUrl,
        body: {
          "username": email,
          "password": password,
        },
      );

      // get responseData from request
      final responseData = json.decode(response.body);
      if (!responseData['success']) {
        //print('***DEV***: responseData : success = ${responseData['success']}');
        //print('***DEV***: responseData : message = ${responseData['message']}');
        throw HttpException(responseData['code'], responseData['message']);
      }

      // wp api jwt response
      _data = responseData['data'].toString();
      _token = responseData['data']['token'].toString();
      _userId = responseData['data']['id'];
      _displayName = responseData['data']['displayName'].toString();
      _email = responseData['data']['email'].toString();
      _tableId = responseData['data']['table_id'];
      // print('**DEV**: ResponseData : data = $_data');
      // print('**DEV**: ResponseData : token = $_token');
      // print('**DEV**: ResponseData : ID = $_userId');
      // print('**DEV**: ResponseData : ID = $_displayName');
      // print('**DEV**: ResponseData : ID = $_displayName');
      // print('**DEV**: ResponseData : ID = $_email');

      print(_displayName);

      notifyListeners();
      FirebaseAnalytics().logEvent(
          name: 'Attempt_To_Login', parameters: {email: email.toString()});
      // save to shared prefs (local storage)
      final prefs = await SharedPreferences.getInstance();

      // wp auth jwt data
      final userData = json.encode(
        {
          'token': _token,
          'userId': _userId,
          'tableId': _tableId,
          'userName': _displayName
        },
      );

      prefs.setString('userData', userData);
    } catch (error) {
      throw error;
    }
  }

  Future<void> login(String email, String password) async {
    // wordpress jwt auth login
    return _authenticate(
      email,
      password,
    );
  }

// autologin function that checs for the token
  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _tableId = extractedUserData['tableId'];
    _displayName = extractedUserData['userName'];
    notifyListeners();
    return true;
  }

  // logout, empty stored token and other stored info
  Future<void> logout() async {
    _token = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}
