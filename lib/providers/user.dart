// Sets contents of user
// fetches users

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../helpers/db_helper.dart';
import '../constants/api.dart' as API;

class User {
  final int userId;
  final String userName;
  final int tableId;

  const User({
    @required this.userId,
    @required this.userName,
    @required this.tableId,
  });
}

class UserProvider with ChangeNotifier {
  final String authToken;
  final int userId;
  List<User> _users = [];

  UserProvider(this.authToken, this.userId, this._users);

  List<User> get users {
    return [..._users];
  }

  User findUser(int userId) {
    return _users.firstWhere((user) => user.userId == userId);
  }

  Future<void> findServerUsers() async {
    print('** USER ** :: Fetching users from server db');
    final url = API.getUsers;
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $authToken',
      });

      final extractedData = json.decode(response.body);
      if (extractedData == null) {
        return;
      }

      for (var i = 0; i < extractedData.length; i++) {
        DBHelper.insert('userList', {
          'userId': extractedData[i]['user_id'],
          'userName': extractedData[i]['nimi'],
          'tableId': extractedData[i]['table_id'],
        });
      }
      print(
          '** USER ** :: Extracted ${extractedData.length} users from server db');
      await fetchLocalUsers();
    } catch (error) {
      inspect(error);
      throw error;
    }
  }

  Future<void> fetchLocalUsers() async {
    print('** USER ** :: Fetching users from local db');
    try {
      final userDb = await DBHelper.getData('userList');
      final List<User> userList = [];
      userDb.forEach((user) => userList.add(User(
            userId: int.parse(user['userId']),
            userName: user['userName'],
            tableId: int.parse(user['tableId']),
          )));
      if (userList.toList().isNotEmpty) {
        _users = userList.toList();
        print('** USER ** :: Found ${_users.length} users in local db');
        notifyListeners();
      } else {
        print('** USER ** :: local user list is empty, load from server');
        await findServerUsers();
      }
    } catch (error) {
      inspect(error);
      throw error;
    }
  }
}
