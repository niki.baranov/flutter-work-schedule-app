import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../constants/api.dart' as API;

class Holidays with ChangeNotifier {
  final String _authToken;
  final int _userId;
  int _holidaysLeft = 0;
  int _holidaysTotal = 0;

  Holidays(
    this._authToken,
    this._userId,
  );

  int get holidaysLeft {
    return _holidaysLeft;
  }

  int get holidaysTotal {
    return _holidaysTotal;
  }

  Future<void> getHolidays() async {
    print('** Holidays ** :: Fetching holidays from server');
    final url = API.getHolidays + _userId.toString();
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $_authToken',
      });

      final extractedData = json.decode(response.body);
      if (extractedData == null) {
        return;
      } else {
        _holidaysLeft = int.parse(extractedData.split('/')[0]);
        _holidaysTotal = int.parse(extractedData.split('/')[1]);
        print(
            '** Holidays ** :: User $_userId has $_holidaysLeft/$_holidaysTotal holidays');
      }
      notifyListeners();
    } catch (error) {
      print('** Holidays ** :: Could not get holidays from server db');
      inspect(error);
      throw error;
    }
  }
}
