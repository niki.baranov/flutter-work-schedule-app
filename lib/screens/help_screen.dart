// shows instrucsions on using the system
// based on the web version

import 'package:flutter/material.dart';

class HelpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //FirebaseAnalytics().logEvent(name: 'Help_Screen', parameters: null);
    return Container(
      padding: const EdgeInsets.all(10),
      child: ListView(
        children: <Widget>[
          ExpansionTile(
            backgroundColor: Colors.blueGrey[50],
            title: Text('Ohjeet työaikojen merkitsemiseen:'),
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        'Kaikki työaikataulukon kellonajat tulee merkitä aina muodossa tt.mm, mukaan lukien poissaolotunnit.'),
                    Divider(),
                    Text('Esimerkkejä:'),
                    Text('Puolitoista tuntia = 1.30'),
                    Text('Aamu yhdeksän = 09.00 tai 9.00'),
                    Text('Puoli viisi = 16.30'),
                  ],
                ),
              ),
            ],
          ),
          ExpansionTile(
            backgroundColor: Colors.blueGrey[50],
            title: Text('Kaikille yhteinen sääntö:'),
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        'Kaikki kirjaavat työssäoloaikansa järjestelmään. Erityisesti etäpäivinä tai matkapäivinä tämä on tärkeää, että työkaverit tietävät milloin ja miten sinut saa kiinni – tai toisaalta myös sen, milloin sinuun ei voi ottaa yhteyttä.'),
                    Divider(),
                    Text(
                        'Kirjaa myös seuraavan päivän tuloaikasi järjestelmään siksi, että muut tietävät koska olet paikalla. Tämä on välttämätöntä paitsi muiden informoimisen takia, myös työmatkatapaturmien vakuutuskorvauksien johdosta.')
                  ],
                ),
              ),
            ],
          ),
          ExpansionTile(
            backgroundColor: Colors.blueGrey[50],
            title: Text('Tuntipalkkalainen:'),
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        'Merkitse järjestelmään päivittäin tekemäsi tunnit, ja saat niistä kaikista seuraavan kuun 5. päivä palkan.'),
                    Divider(),
                    Text(
                        'Lähtökohta on se, että kenenkään ei tarvitse tehdä enempää kuin työsopimuksessa mainittu tuntimäärä, ja loman tarpeet kuittaantuisivat vuosilomilla. Jos sinulle jossain kuussa on kertynyt paljon ylimääräisiä tunteja, ja haluat niistä vapaata, voit ottaa yhteyttä Anneen tai Hennan ja sopia asiasta.')
                  ],
                ),
              ),
            ],
          ),
          ExpansionTile(
            backgroundColor: Colors.blueGrey[50],
            title: Text('Kuukausipalkkalainen:'),
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        'Työnantaja ei puutu tuntitasolla päivittäistä työaikaasi.'),
                    Divider(),
                    Text(
                        'Etätyötä saa tehdä oman harkinnan mukaan – mielellään kuitenkin enintään yksi päivä per viikko. Jos joskus on tarve tehdä suuremmalle etätyömäärälle, sovi tästä Annen tai Hennan kanssa.'),
                    Text(
                        'Työpäivien määrää seurataan. Lomat ovat siis lomia, ja niitä tarvitsee pitää.'),
                  ],
                ),
              ),
            ],
          ),
          ExpansionTile(
            backgroundColor: Colors.blueGrey[50],
            title: Text('Työmatkaohjeet:'),
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        'Tuntipalkkalaiset: Työaikalaissa lähdetään siitä, että matkustamiseen käytetty aika ei ole työaikaa. Näin siitä huolimatta, vaikka työntekijä matkustaisi työsopimuksessa sovittujen työtehtävien hoitamiseksi.  Jos työntekijän tehtävä on esim. kuljettaa tavaraa toiselle paikkakunnalle, silloin auton ajaminen on määrättyä työtä ja siihen käytetty aika työaikaa. “Kartturina” toimiminen ei.'),
                    Divider(),
                    Text(
                        'Kaikki: Myös viikonlopputyöstä on sovittu siitä maksettavan normaalipalkka tai että työntekijä pitää saman tuntimäärän vapaata mieluimmin välittömästi viikonlopputyöstä vapauduttuaan.'),
                    Text(
                        'Päivärahoja laskettaessa työ- ja matka-ajat lasketaan kuitenkin aina yhteen, vaikka matka-ajan olisikin toiminut kartturina, nukkunut tai tehnyt töitä.'),
                    Divider(),
                    Text(
                        'Työntekijä voi jäädä (tai mennä etukäteen) vapaa-ajallaan työntekopaikkakunnalle ja siitä huolimatta työnantaja maksaa myöhemmin tehdyn kotimatkan kustannukset. Matkalaskuun matkustusaika merkitään kuitenkin sen mukaan, milloin työntekijä olisi palannut matkalta ilman tätä itse haluamaansa “kyläilyaikaa”.'),
                  ],
                ),
              ),
            ],
          ),
          ExpansionTile(
            backgroundColor: Colors.blueGrey[50],
            title: Text('Sairastuminen:'),
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        'Ilmoita sairastumisesta Annelle mieluiten soittamalla.'),
                    Divider(),
                    Text(
                        'Ilman lääkärintodistusta voi olla pois 3 päivää, mutta toiveena on, että vakavammissa tapauksissa menet heti lääkärille. Työnantajakin saa pidemmissä poissaoloissa korvauksen Kelalta vain lääkärintodistusta vastaan.'),
                    Text(
                        'Äkillisestä sairastumisesta johtuva kesken työpäivän tapahtuva lääkärissäkäynti voidaan katsoa työajaksi. Jos taas lääkäri tai vaikka fysioterapeutti on varattu etukäteen ja käynti on kiireetön, silloin sinne mennään omalla ajalla.'),
                    Divider(),
                    Text(
                        'Jos työntekijällä on alle 10-vuotias lapsi ja tämä sairastuu äkillisesti, äidin tai isän on työsopimuslain mukaan oikeus jäädä kotiin hoitamaan lasta, ellei hoito muuten järjesty.'),
                    Divider(),
                    Text(
                        'Kotiin voi jäädä sairastuneen lapsen takia enintään neljäksi päiväksi.'),
                    Divider(),
                    Text(
                        'Työnantaja maksaa palkkaa kolmelta päivältä sairastumispäivä mukaan lukien.'),
                    Divider(),
                    Text(
                        'Jos myös toinen lapsi sairastuu, kyse on uudesta jaksosta ja työntekijälle voidaan jälleen maksaa palkkaa kolmelta poissaolopäivältä.'),
                    Divider(),
                    Text(
                        'Osa-aikaisten työntekijöiden suhteen lähtökohta on, että edellisellä viikolla on seuraavan viikon työpäivät sovittuna. Jos siis osa-aikainen sairastuu, työpäiviksi sovituilta päiviltä hän saa palkan, muilta ei.'),
                    Divider(),
                    Text(
                        'Työnantaja maksaa palkkaa kolmelta päivältä sairastumispäivä mukaan lukien.'),
                    Divider(),
                    Text(
                        'Pidemmissä poissaoloissa sovelletaan keskiarvoa aikaisemmin toteutuneista tunneista – siis niin, että palkan saa esim. kolme päivää/kuussa -säännön mukaan, mikäli sairaslomaa edeltäneen neljännesvuoden aikana tämä on ollut keskimääräinen työssäoloaika.'),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
