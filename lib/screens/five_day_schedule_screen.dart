// shows a grid of users for selected day
// shows name, location and colors the grid of each user
// according to the location

import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../main.dart';
import 'package:provider/provider.dart';

import '../providers/five_day.dart';
import '../providers/schedule.dart';
import '../providers/user.dart';
import '../providers/day.dart';
import '../screens/splash_screen.dart';
import '../widgets/five_day_schedule.dart';

class FiveDayScheduleScreen extends StatefulWidget {
  static const routeName = '/five-day-schedule';

  @override
  _FiveDayScheduleScreenState createState() => _FiveDayScheduleScreenState();
}

class _FiveDayScheduleScreenState extends State<FiveDayScheduleScreen> {
  var _isLoadingArrived = false;
  var _isLoadingLeft = false;
  Future<void> _refreshFiveDay(BuildContext context) async {
    print('** 5 DAY SCREEN ** :: refresh from server');
    await Provider.of<FiveDay>(context, listen: false).fetchFiveDayFromServer();
  }

  Future<void> _refreshLocalFiveDay(BuildContext context) async {
    print('** 5 DAY SCREEN ** :: refresh from local db');
    await Provider.of<FiveDay>(context, listen: false)
        .fetchFiveDayFromLocalDb();
  }

  @override
  Widget build(BuildContext context) {
    //FirebaseAnalytics().logEvent(name: 'Five_Day_Screen', parameters: null);
    final scaffold = Scaffold.of(context);
    print('** 5 DAY SCREEN ** :: building five day schedule');

    return Scaffold(
      backgroundColor: AppColors.bg,
      body: FutureBuilder(
        future:
            Provider.of<UserProvider>(context, listen: false).fetchLocalUsers(),
        // initialData: Provider.of<UserProvider>(context, listen: false).users,
        builder: (ctx, snapshot) =>
            snapshot.connectionState == ConnectionState.waiting
                ? SplashScreen()
                : FutureBuilder(
                    future: _refreshLocalFiveDay(ctx),
                    // initialData: Provider.of<FiveDay>(context, listen: false).fiveday,
                    builder: (ctx, snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? SplashScreen()
                            : RefreshIndicator(
                                onRefresh: () => _refreshFiveDay(context),
                                child: Consumer<FiveDay>(
                                  builder: (ctx, scheduleData, child) =>
                                      ListView.builder(
                                    itemCount: scheduleData.users.length,
                                    itemBuilder: (ctx, index) {
                                      List<Day> fiveDaySchedule =
                                          scheduleData.findFiveNextDays(
                                              scheduleData.users[index].userId);

                                      if (fiveDaySchedule.length >= 1) {
                                        return FiveDaySchedule(
                                            scheduleData.users[index],
                                            fiveDaySchedule);
                                      } else {
                                        return Container();
                                      }
                                    },
                                  ),
                                ),
                              ),
                  ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FlatButton.icon(
            icon: Icon(Icons.work),
            label: _isLoadingArrived
                ? Container(
                    width: 48,
                    padding: const EdgeInsets.all(0),
                    child: Center(
                        child: SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator())),
                  )
                : Text('Saavuin'),
            color: AppColors.arrivedAtButton,
            onPressed: () async {
              setState(() {
                _isLoadingArrived = true;
              });
              try {
                await Provider.of<Schedule>(context, listen: false)
                    .fetchLocalSchedule();
                await Provider.of<Schedule>(context, listen: false)
                    .updateTime('arrivedAt');
                // update today list
                await Provider.of<FiveDay>(context, listen: false)
                    .fetchFiveDayFromServer();
                scaffold
                  ..removeCurrentSnackBar()
                  ..showSnackBar(SnackBar(
                      content: Text(
                          'Saapumisaika päivitetty : ${TimeOfDay.now().hour}:${TimeOfDay.now().minute}')));
              } catch (error) {}
              setState(() {
                _isLoadingArrived = false;
              });
            },
          ),
          FlatButton.icon(
            icon: Icon(Icons.home),
            label: _isLoadingLeft
                ? Container(
                    width: 48,
                    padding: const EdgeInsets.all(0),
                    child: Center(
                        child: SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white),
                            ))),
                  )
                : Text('Lähdin'),
            color: AppColors.leftAt,
            onPressed: () async {
              setState(() {
                _isLoadingLeft = true;
              });
              try {
                await Provider.of<Schedule>(context, listen: false)
                    .fetchLocalSchedule();
                await Provider.of<Schedule>(context, listen: false)
                    .updateTime('leftAt');
                // update today list
                await Provider.of<FiveDay>(context, listen: false)
                    .fetchFiveDayFromServer();
                scaffold
                  ..removeCurrentSnackBar()
                  ..showSnackBar(SnackBar(
                      content: Text(
                          'Lähtöaika päivitetty : ${TimeOfDay.now().hour}:${TimeOfDay.now().minute}')));
              } catch (error) {}
              setState(() {
                _isLoadingLeft = false;
              });
            },
          ),
        ],
      ),
    );
  }
}
