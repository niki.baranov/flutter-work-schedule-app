import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  static const routeName = '/splash-screen';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Container(
          height: deviceSize.height,
          width: deviceSize.width,
          color: Colors.blueGrey[600],
          child: Column(
            children: <Widget>[
              //Image(image: AssetImage('assets/logo/taj_icon.png')),
              CircularProgressIndicator(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Ladataan aikatauluja...',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
