import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRangePicker;

import '../screens/splash_screen.dart';
import '../screens/five_day_schedule_screen.dart';
import '../screens/help_screen.dart';
import '../screens/notifications_screen.dart';
import '../screens/schedule_screen.dart';
import '../widgets/app_drawer.dart';

import '../main.dart';

class TabsScreen extends StatefulWidget {
  static const routeName = '/tabs';
  //final int userId;
  // final int tableId;
  //TabsScreen(this.userId);

  // final List<Schedule> schedule;
  // final List<Schedule> usersSchedule;

  // TabsScreen(this.schedule, this.usersSchedule);

  //final List<Day> nextFiveDays;

  //TabsScreen(this.nextFiveDays);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  var _isInit = true;
  var _isLoading = false;
  // init a list of pages
  //List<Map<String, Object>> _pages;
  // initial page index
  int _selectedPageIndex = 1;

  List<DateTime> dates = [null, null];
  List<Map<String, Object>> _pages;

  // @override
  // void initState() {
  //   //print('initState');
  //   // print('tab screen tableid: ${widget.tableId}');
  //   // print('tab screen userId: ${widget.userId}');
  //   // list of pages for the bottomNavBar

  //   super.initState();
  // }

  @override
  void didChangeDependencies() {
    _pages = [
      {'page': ScheduleScreen(dates[0], dates[1]), 'title': ''},
      {
        'page': FiveDayScheduleScreen(),
        'title': 'Tänään (5 päivää)',
        'actions': [logoutButton()],
      },
      {
        'page': NotificationsScreen(),
        'title': 'Ilmoitukset',
        'actions': [logoutButton()],
      },
      {
        'page': HelpScreen(),
        'title': 'Ohjeet',
        'actions': [logoutButton()],
      },
    ];

    print('** TABS SCREEN ** :: didChangeDependencies');
    if (_isInit) {
      print('** TABS SCREEN ** :: _isInit = true');
      setState(() {
        _isLoading = false;
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  void _selectPage(int index) async {
    if (!mounted) return;
    setState(() {
      _selectedPageIndex = index;
    });
    await FirebaseAnalytics().logEvent(name: 'Screen', parameters: {
      'screen': _pages[index]['title'] != ""
          ? '${_pages[index]['title']}'
          : 'Full Schedule'
    });
  }

  //DateTime _startDate;
  DateTime _startDate = DateTime(DateTime.now().year, DateTime.now().month, 1);
  //DateTime _endDate;
  DateTime _endDate =
      DateTime(DateTime.now().year, DateTime.now().month + 1, 0);

  Future displayDateRangePicker(BuildContext context) async {
    final List<DateTime> picked = await DateRangePicker.showDatePicker(
      context: context,
      initialFirstDate: dates[0] == null ? _startDate : dates[0],
      initialLastDate: dates[1] == null ? _endDate : dates[1],
      firstDate: new DateTime(DateTime.now().year - 50),
      lastDate: new DateTime(DateTime.now().year + 50),
      locale: Locale('fi', 'FI'),
    ).then((picked) {
      if (picked != null && picked.length == 2) {
        setState(() {
          _startDate = picked[0];
          _endDate = picked[1];

          dates[0] = picked[0];
          dates[1] = picked[1];
        });
      }

      return dates;
    });
  }

  Widget logoutButton() {
    return Platform.isIOS
        ? SizedBox()
        : IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              //exit(0);
              SystemChannels.platform.invokeMethod('SystemNavigator.pop');
              //Navigator.of(context).pop();
              //Provider.of<Auth>(context, listen: false).logout();
              //Navigator.of(context).pushReplacementNamed(SplashScreen.routeName);
            },
          );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    if (dates[0] == null || dates[1] == null) {
      dates[0] = _startDate;
      dates[1] = _endDate;
    }
    // rebuild schedule screen dates
    _pages[0] = {'page': ScheduleScreen(dates[0], dates[1]), 'title': ''};

    //var provider = Provider.of<BottomNavProvider>(context);
    return DefaultTabController(
      length: 4,
      initialIndex: 2,
      child: Scaffold(
        backgroundColor: Colors.blueGrey[100],
        // backgroundColor: Colors.blueGrey[900],
        // backgroundColor: Colors.black,
        appBar: AppBar(
          // hardcoded title
          //title: Text('Työaikajärjestelmä'),

          // dynamic title
          title: Text(_pages[_selectedPageIndex]['title']),
          //title: Text(_pages[provider.currentIndex]['title']),
          actions: _selectedPageIndex == 0
              ? <Widget>[
                  FlatButton(
                    child: Text(
                      dates[0] != null
                          ? '${DateFormat('dd.MM').format(dates[0]).toString()} - ${DateFormat('dd.MM.y').format(dates[1]).toString()}'
                          : '${DateFormat('dd.MM').format(_startDate).toString()} - ${DateFormat('dd.MM.y').format(_endDate).toString()}',
                      style: TextStyles.dateRangeSelector,
                    ),
                    // Text(
                    //   '${DateFormat('d.MM').format(_startDate).toString()} - ${DateFormat('d.MM.y').format(_endDate).toString()}',
                    // ),
                    onPressed: () async {
                      await displayDateRangePicker(context);
                      setState(() {
                        _startDate = dates[0];
                        _endDate = dates[1];
                      });
                      dates == null
                          ? print('** TABS SCREEN ** :: dates is null')
                          : print(
                              '** TABS SCREEN ** :: dates not null : ${dates[0]}');
                    },
                  ),
                  logoutButton()
                ]
              : _pages[_selectedPageIndex]['actions'],
          //_pages[provider.currentIndex]['actions'],

          // bottom: TabBar(
          //   labelPadding: const EdgeInsets.all(0),
          //   isScrollable: false,
          //   tabs: <Widget>[
          //     // schedule
          //     Tab(
          //       icon: Icon(Icons.calendar_today),
          //       text: 'Työaika',
          //     ),
          //     // today
          //     Tab(
          //       icon: Icon(Icons.today),
          //       text: 'Tanaan',
          //     ),
          //     // notifications
          //     Tab(
          //       icon: Icon(Icons.notification_important),
          //       text: 'Ilmoitukset',
          //     ),
          //     // help/info
          //     Tab(
          //       icon: Icon(Icons.help_outline),
          //       text: 'Ohje',
          //     )
          //   ],
          // ),
        ),
        drawer: AppDrawer(),

        body: Container(
          height: deviceSize.height,
          width: deviceSize.width,
          child:
              _isLoading ? SplashScreen() : _pages[_selectedPageIndex]['page'],
        ),

        // another type of tabbar
        // body: TabBarView(
        //   children: [
        //     _pages[0]['page'],
        //     _pages[1]['page'],
        //     _pages[2]['page'],
        //     _pages[3]['page'],
        //   ],
        // ),

        //body: _pages[provider.currentIndex]['page'],

        // only show floating buttons on 5 day schedule
        //floatingActionButton: showFloatingButton(),
        bottomNavigationBar:

            // another type of tabbar
            // Container(
            //   color: Colors.blueGrey[900],
            //   child: TabBar(
            //     labelColor: Theme.of(context).accentColor,
            //     unselectedLabelColor: Colors.white,
            //     indicatorPadding: EdgeInsets.all(5),
            //     indicatorColor: Theme.of(context).accentColor,
            //     labelPadding: EdgeInsets.only(left: 5, right: 5),
            //     tabs: [
            //       Tab(
            //         icon: Icon(Icons.schedule),
            //         iconMargin: EdgeInsets.all(2),
            //         text: 'Oma Työaika',
            //       ),
            //       Tab(
            //         icon: Icon(Icons.today),
            //         iconMargin: EdgeInsets.all(2),
            //         text: 'Tänään',
            //       ),
            //       Tab(
            //         icon: Icon(Icons.notifications),
            //         iconMargin: EdgeInsets.all(2),
            //         text: 'Ilmoitukset',
            //       ),
            //       Tab(
            //         icon: Icon(Icons.help),
            //         iconMargin: EdgeInsets.all(2),
            //         text: 'Ohje',
            //       ),
            //     ],
            //   ),
            // ),

            BottomNavigationBar(
          onTap: _selectPage,
          //  (index) {
          //   provider.currentIndex = index;
          // },

          backgroundColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.white,
          selectedItemColor: Theme.of(context).accentColor,
          currentIndex: _selectedPageIndex,
          //currentIndex: provider.currentIndex,
          type: BottomNavigationBarType.shifting,
          items: [
            BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.schedule),
              title: Text('Oma Työaika'),
            ),
            BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.today),
              title: Text('Tänään'),
            ),
            BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.notifications),
              title: Text('Ilmoitukset'),
            ),
            BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.help),
              title: Text('Ohje'),
            ),
            // BottomNavigationBarItem(
            //   backgroundColor: Theme.of(context).primaryColor,
            //   icon: Icon(Icons.help),
            //   title: Text('Ohje'),
            // ),
          ],
        ),

        // body: TabBarView(
        //   // prevents tabbar scrolling
        //   //physics: NeverScrollableScrollPhysics(),
        //   children: <Widget>[
        //     ScheduleScreen(),
        //     TodayScreen(),
        //     NotificationsScreen(),
        //     HelpScreen()
        //   ],
        // ),
      ),
    );
  }
}
