// opens up a screen to edit info set for the selected day
// activated from schedule_screen by tapping a day
// has save/cancel buttons
// save button saves new details and returns to the schedule_screen
// cancel button ignores changes and returns to schedule_screen

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../providers/day.dart';
import '../providers/schedule.dart';
import '../main.dart';

class EditSchedule extends StatefulWidget {
  static const routeName = '/edit-schedule';

  @override
  _EditScheduleState createState() => _EditScheduleState();
}

class _EditScheduleState extends State<EditSchedule> {
  final _unavailableController = TextEditingController();
  final _otherInfoController = TextEditingController();
  final _messageController = TextEditingController();

  final _otherFocusNode = FocusNode();
  final _messageFocusNode = FocusNode();
  final _unavailableFocusNode = FocusNode();

  // unique gloabal key for form
  final _form = GlobalKey<FormState>();

  // empty day object to store data from selected day
  var _editedDay = EditedDay(
    dayId: null,
    userId: null,
    date: null,
    location: null,
    estimate: null,
    arrivedAt: null,
    leftAt: null,
    unavailable: '',
    other: '',
    message: '',
    updatedAt: '',
  );

  var _isInit = true;
  var _isLoading = false;

  Location dropDownValue;
  TimeOfDay _selectedTime;

  List<String> thisDate;
  List<String> endOfWeek;
  List<String> endOfMonth;
  List<String> _selectedRange;

  // finds the end date for set range
  List<String> setDateRange(String day, String type) {
    DateTime _startDate = DateTime.parse(day);
    String _endDate;
    if (type == 'day') {
      _endDate = null;
    }
    if (type == 'week') {
      int _daysToFriday = 5 - _startDate.weekday;
      _endDate = _startDate.add(Duration(days: _daysToFriday)).toString();
      _endDate =
          DateFormat('y-MM-dd').format(DateTime.parse(_endDate)).toString();
    }
    if (type == 'month') {
      _endDate = DateTime(_startDate.year, _startDate.month + 1, 0).toString();
      _endDate =
          DateFormat('y-MM-dd').format(DateTime.parse(_endDate)).toString();
    }

    return [DateFormat('y-MM-dd').format(_startDate).toString(), _endDate];
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      // checks if product id was passed
      final dayId = ModalRoute.of(context).settings.arguments as int;
      // if (dayId != null) {
      final day = Provider.of<Schedule>(context, listen: true).findById(dayId);

      _editedDay = EditedDay(
        dayId: day.dayId,
        userId: day.userId,
        date: day.date,
        location: day.location,
        estimate: day.estimate,
        arrivedAt: day.arrivedAt,
        leftAt: day.leftAt,
        unavailable: day.unavailable,
        other: day.other,
        message: day.message,
        updatedAt: day.updatedAt,
      );

      _unavailableController.text = day.unavailable;
      _otherInfoController.text = day.other;
      _messageController.text = day.message;

      // this date
      thisDate = setDateRange(_editedDay.date, 'day');
      // end of week date
      endOfWeek = setDateRange(_editedDay.date, 'week');
      // end of month date
      endOfMonth = setDateRange(_editedDay.date, 'month');
      //_selectedRange = _editedDay.date.toString();
      _selectedRange = thisDate;
      print('** SCHEUDLE EDIT ** :: selected days: $_selectedRange');

      // _initialValues = {
      //   'dayId': _editedDay.dayId.toString(),
      //   'userId': _editedDay.userId.toString(),
      //   'userName': _editedDay.userName,
      //   'date': _editedDay.date,
      //   'location': _editedDay.location.location,
      //   'estimate': _editedDay.estimate.toString(),
      //   'arrivedAt': _editedDay.arrivedAt.toString(),
      //   'leftAt': _editedDay.leftAt.toString(),
      //   'unavailable': _editedDay.unavailable,
      //   'other': _editedDay.other,
      //   'message': _editedDay.message,
      // };
      //}
    }
    _isInit = false;
    super.didChangeDependencies();
  }

// select time with switch case for each type
  Future<TimeOfDay> selectTime(
    //TimeOfDay time,
    String type,
    BuildContext context,
    TimeOfDay loadedTime,
  ) async {
    TimeOfDay newTime = await showTimePicker(
      context: context,
      initialTime: loadedTime != null ? loadedTime : TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    ).then((newTime) {
      if (newTime == null) {
        print('** SCHEUDLE EDIT ** :: cancelled');
        return;
      }
      _selectedTime = newTime;
      setState(() {
        switch (type) {
          case 'estimate':
            _editedDay.estimate = _selectedTime;
            break;
          case 'arrivedAt':
            _editedDay.arrivedAt = _selectedTime;
            break;
          case 'leftAt':
            _editedDay.leftAt = _selectedTime;
            break;
        }
        // update _editedDay with new value
      });
    });
    return newTime;
  }

  // covert minutes to double digit if needed
  String doubleDigit(String minute) {
    if (int.parse(minute) < 10) {
      minute = '0' + minute;
    }
    return minute;
  }

  Widget timeSelector(
    String type,
    String label,
    TimeOfDay loadedTime,
  ) {
    var timeType;
    Color timeColor;
    IconData timeIcon;
    switch (type) {
      case 'estimate':
        timeType = _editedDay.estimate;
        timeColor = AppColors.estimate;
        timeIcon = Icons.access_time;
        break;
      case 'arrivedAt':
        timeType = _editedDay.arrivedAt;
        timeColor = AppColors.arrivedAt;
        timeIcon = Icons.timer;
        break;
      case 'leftAt':
        timeType = _editedDay.leftAt;
        timeColor = AppColors.leftAt;
        timeIcon = Icons.timer_off;
        break;
      default:
    }

    return Padding(
      padding: const EdgeInsets.only(top: 4, bottom: 4),
      child: Container(
        decoration: BoxDecoration(
            color: AppColors.textFieldBg,
            borderRadius: BorderRadius.circular(6),
            border: Border.all(width: 1, color: Colors.grey)),
        padding: EdgeInsets.only(top: 0, bottom: 0, left: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    timeIcon,
                    color: AppColors.formFieldIcon,
                  ),
                  SizedBox(width: 12),
                  Text(
                    label[0].toUpperCase() + label.substring(1) + ':',
                    style: TextStyles.formFieldLabel,
                  ),
                ],
              ),
            ),
            loadedTime == null
                ? GestureDetector(
                    onTap: () {
                      print('loadedTime == null');
                      selectTime(
                        type,
                        context,
                        loadedTime,
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 3),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.redAccent),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.add_alarm,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : GestureDetector(
                    onTap: () {
                      print('loadedTime != null');
                      selectTime(
                        type,
                        context,
                        loadedTime,
                      );
                    },
                    child: Container(
                      width: 110,
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: timeColor,
                        borderRadius: const BorderRadius.only(
                            bottomRight: Radius.circular(5),
                            topRight: Radius.circular(5)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          IconButton(
                              padding: const EdgeInsets.only(right: 20),
                              constraints:
                                  BoxConstraints(minHeight: 0, minWidth: 0),
                              icon: Icon(
                                Icons.delete_forever,
                                size: 26,
                              ),
                              onPressed: () {
                                setState(() {
                                  loadedTime = null;
                                  timeType = null;
                                  _selectedTime = null;
                                  switch (type) {
                                    case 'estimate':
                                      _editedDay.estimate = null;
                                      break;
                                    case 'arrivedAt':
                                      _editedDay.arrivedAt = null;
                                      break;
                                    case 'leftAt':
                                      _editedDay.leftAt = null;
                                      break;
                                    default:
                                  }
                                });
                              }),
                          Text(
                            (timeType != loadedTime && timeType != null)
                                ? '${timeType.hour}:${doubleDigit(timeType.minute.toString())}'
                                : '${loadedTime.hour}:${doubleDigit(loadedTime.minute.toString())}',
                            style: TextStyles.formTime,
                          ),
                        ],
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _otherFocusNode.dispose();
    _messageFocusNode.dispose();
    _unavailableFocusNode.dispose();
    super.dispose();
  }

  void _saveForm() async {
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });

    if (_editedDay.dayId != null) {
      await Provider.of<Schedule>(context, listen: false).updateDay(
        _editedDay.dayId,
        Day(
          dayId: _editedDay.dayId,
          userId: _editedDay.userId,
          date: _editedDay.date,
          location: _editedDay.location,
          estimate: _editedDay.estimate,
          arrivedAt: _editedDay.arrivedAt,
          leftAt: _editedDay.leftAt,
          unavailable: _editedDay.unavailable,
          other: _editedDay.other,
          message: _editedDay.message,
          updatedAt: DateTime.now().toString(),
        ),
        _selectedRange[0],
        _selectedRange[1],
      );

      if (_selectedRange[1] != null) {
        Navigator.of(context).pop(
            'Muutokset aikavälille ${DateFormat('dd').format(DateTime.parse(_selectedRange[0])).toString()} - ${DateFormat('dd. MMMM').format(DateTime.parse(_selectedRange[1])).toString()} tallennettu');
      } else {
        // closes edit screen, returns to schedule
        Navigator.of(context).pop(
            'Muutokset päivälle ${DateFormat('dd. MMMM').format(DateTime.parse(_editedDay.date)).toString()} tallennettu');
      }
    } else {
      print('** EDIT SCHEUDLE ** :: ...');
    }
    setState(() {
      _isLoading = false;
    });

    // print('dayId: ${_editedDay.dayId}');
    // print('userId: ${_editedDay.userId}');
    // print('date: ${_editedDay.date}');
    // print('location: ${_editedDay.location}');
    // print('estimate: ${_editedDay.estimate}');
    // print('arrivedAt: ${_editedDay.arrivedAt}');
    // print('leftAt: ${_editedDay.leftAt}');
    // print('unavailable: ${_editedDay.unavailable}');
    // print('other: ${_editedDay.other}');
    // print('message: ${_editedDay.message}');
  }

// checks if its weekend
  _isWeekEnd() {
    if (DateTime.parse(_editedDay.date).weekday == 6 ||
        DateTime.parse(_editedDay.date).weekday == 7) {
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    //FirebaseAnalytics().logEvent(name: 'Edit_Schedule_Screen', parameters: null);
    final deviceSize = MediaQuery.of(context).size;

    //print(loadedDay.location);
    // print('--- _editedDay contents ---');
    // print('dayId: ${_editedDay.dayId}');
    // print('userId: ${_editedDay.userId}');
    // print('date: ${_editedDay.date}');
    // print('location: ${_editedDay.location}');
    // print('estimate: ${_editedDay.estimate}');
    // print('arrivedAt: ${_editedDay.arrivedAt}');
    // print('leftAt: ${_editedDay.leftAt}');
    // print('unavailable: ${_editedDay.unavailable}');
    // print('other: ${_editedDay.other}');
    // print('message: ${_editedDay.message}');

    return Scaffold(
      backgroundColor: Colors.blueGrey[50],
      appBar: AppBar(
        title: Text(toBeginningOfSentenceCase(DateFormat('MMMMEEEEd')
            .format(DateTime.parse(_editedDay.date))
            .toString())),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Form(
          key: _form,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                // location row
                Container(
                  decoration: BoxDecoration(
                      color: AppColors.textFieldBg,
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(width: 1, color: Colors.grey)),
                  padding: const EdgeInsets.only(left: 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          'Sijainti :',
                          style: TextStyles.formFieldLabel,
                        ),
                      ),
                      Container(
                        width: 200,
                        padding: const EdgeInsets.all(3),
                        child: DropdownButtonFormField<String>(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(
                                right: 0, top: 8, left: 5, bottom: 8),
                            isDense: true,
                            prefixStyle: TextStyle(fontSize: 30),
                            suffixStyle: TextStyle(fontSize: 30),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(),
                            ),
                          ),
                          value: _editedDay.location.location,
                          items: Location.values
                              .map<DropdownMenuItem<String>>((Location value) {
                            return DropdownMenuItem<String>(
                              value: value.location,
                              child: Text(value.location.toString(),
                                  style: TextStyles.formFieldLabel),
                            );
                          }).toList(),
                          onChanged: (String newValue) {
                            setState(() {
                              _editedDay.location = Location.values.firstWhere(
                                  (l) => l.location.toString() == newValue);
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),

                // date range row
                Padding(
                  padding: const EdgeInsets.only(top: 4, bottom: 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: (deviceSize.width / 4),
                        decoration: BoxDecoration(
                            color: AppColors.textFieldBg,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(width: 1, color: Colors.grey)),
                        padding: const EdgeInsets.only(left: 4, right: 4),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.all(4),
                              child: Text(
                                'Tämä päivä',
                                style: TextStyles.formFieldLabel,
                              ),
                            ),
                            Radio(
                                value: thisDate,
                                groupValue: _selectedRange,
                                onChanged: (val) {
                                  setState(() {
                                    _selectedRange = val;
                                  });
                                }),
                          ],
                        ),
                      ),
                      Container(
                        width: (deviceSize.width / 4),
                        decoration: BoxDecoration(
                            color: AppColors.textFieldBg,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(width: 1, color: Colors.grey)),
                        padding: const EdgeInsets.only(left: 4, right: 4),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.all(4),
                              child: Text(
                                'Viikko',
                                style: TextStyles.formFieldLabel,
                              ),
                            ),
                            Radio(
                                value: endOfWeek,
                                groupValue: _selectedRange,
                                onChanged: _isWeekEnd()
                                    ? null
                                    : (val) {
                                        setState(() {
                                          _selectedRange = val;
                                        });
                                      }),
                          ],
                        ),
                      ),
                      Container(
                        width: (deviceSize.width / 4),
                        decoration: BoxDecoration(
                            color: AppColors.textFieldBg,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(width: 1, color: Colors.grey)),
                        padding: const EdgeInsets.only(left: 4, right: 4),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.all(4),
                              child: Text(
                                'Kuukausi',
                                style: TextStyles.formFieldLabel,
                              ),
                            ),
                            Radio(
                                value: endOfMonth,
                                groupValue: _selectedRange,
                                onChanged: _isWeekEnd()
                                    ? null
                                    : (val) {
                                        setState(() {
                                          _selectedRange = val;
                                        });
                                      }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),

                // Time rows
                _editedDay.location == Location.Remote ||
                        _editedDay.location == Location.Toimistolla ||
                        _editedDay.location == Location.WorkTrip
                    ? Container(
                        padding: const EdgeInsets.only(top: 3),
                        child: Column(
                          children: <Widget>[
                            timeSelector(
                              'estimate',
                              'arvioitu aloitusaika',
                              _editedDay.estimate,
                            ),
                            timeSelector(
                              'arrivedAt',
                              'aloitusaika',
                              _editedDay.arrivedAt,
                            ),
                            timeSelector(
                              'leftAt',
                              'lopetusaika',
                              _editedDay.leftAt,
                            ),
                            Divider(),
                          ],
                        ),
                      )
                    : SizedBox(
                        width: 0,
                        height: 0,
                      ),

                // unavailable field
                Container(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: _unavailableController,
                          style: TextStyles.formFieldLabel,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.textFieldBg,
                            labelText: 'Poissa',
                            prefixIcon: Icon(
                              Icons.highlight_off,
                              color: AppColors.formFieldIcon,
                            ),
                            contentPadding: const EdgeInsets.all(5),
                            border:
                                OutlineInputBorder(borderSide: BorderSide()),
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.lightGreen)),
                          ),
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.number,
                          onSaved: (newUnavailable) {
                            _editedDay.unavailable = newUnavailable;
                          },
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          _unavailableController.clear();
                        },
                        color: AppColors.deleteButtonIcon,
                      )
                    ],
                  ),
                ),

                // other field
                Container(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: _otherInfoController,
                          style: TextStyles.formFieldLabel,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.textFieldBg,
                            labelText: 'Muuta',
                            prefixIcon: Icon(
                              Icons.info,
                              color: AppColors.formFieldIcon,
                            ),
                            contentPadding: const EdgeInsets.all(5),
                            border:
                                OutlineInputBorder(borderSide: BorderSide()),
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.lightGreen)),
                          ),
                          textInputAction: TextInputAction.done,
                          onSaved: (newOther) {
                            _editedDay.other = newOther;
                          },
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          _otherInfoController.clear();
                        },
                        color: AppColors.deleteButtonIcon,
                      )
                    ],
                  ),
                ),

                // message field
                Container(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: _messageController,
                          style: TextStyles.formFieldLabel,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.textFieldBg,
                            labelText: 'Viesti muille',
                            prefixIcon: Icon(
                              Icons.message,
                              color: AppColors.formFieldIcon,
                            ),
                            contentPadding: const EdgeInsets.all(5),
                            border:
                                OutlineInputBorder(borderSide: BorderSide()),
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.lightGreen)),
                          ),
                          textInputAction: TextInputAction.done,
                          onSaved: (newMessage) {
                            _editedDay.message = newMessage;
                          },
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          _messageController.clear();
                        },
                        color: AppColors.deleteButtonIcon,
                      )
                    ],
                  ),
                ),

                // cancel and save buttons
                if (_isLoading)
                  Container(
                    height: 30,
                    child: Center(
                        child: SizedBox(
                            height: 26,
                            width: 26,
                            child: CircularProgressIndicator())),
                  )
                else
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RaisedButton.icon(
                          color: AppColors.cancelButton,
                          elevation: 3,
                          icon: Icon(
                            Icons.exit_to_app,
                            color: AppColors.cancelButtonIcon,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop(
                                'Muutokset päivälle ${DateFormat('dd. MMMM').format(DateTime.parse(_editedDay.date)).toString()} hylätty');
                          },
                          label: Text(
                            'Peruuta',
                            style: TextStyles.cancelButtonText,
                          )),
                      RaisedButton.icon(
                        elevation: 3,
                        icon: Icon(
                          Icons.save,
                          color: AppColors.saveButtonIcon,
                        ),
                        label:
                            Text('Tallenna', style: TextStyles.saveButtonLabel),
                        color: AppColors.saveButton,
                        onPressed: () {
                          _saveForm();
                        },
                      ),
                    ],
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
