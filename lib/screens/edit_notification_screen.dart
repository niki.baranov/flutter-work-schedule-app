import 'package:flutter/material.dart';
import '../providers/notifications.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class EditNotification extends StatefulWidget {
  static const routeName = '/edit-notification';

  @override
  _EditNotificationState createState() => _EditNotificationState();
}

class _EditNotificationState extends State<EditNotification> {
  // unique gloabal key for form
  final _form = GlobalKey<FormState>();
  // controller for notification message field
  final _notificationMessageController = TextEditingController();
  final _notificationMessageFocusNode = FocusNode();
  // placeholder for notification
  var _editedNotification = NotificationMessage(
    notificationId: null,
    ownerId: null,
    notificationMessage: '',
    lastUpdate: null,
  );

  var _initValues = {
    'notificationId': int,
    'ownerId': null,
    'notificationMessage': '',
    'lastUpdate': DateTime,
  };

  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    print('** EDIT NOTIFICATION SCREEN ** :: didchange happened');
    if (_isInit) {
      final notificationId = ModalRoute.of(context).settings.arguments as int;
      if (notificationId != null) {
        final notification =
            Provider.of<NotificationProvider>(context, listen: true)
                .findById(notificationId);
        _editedNotification = notification;
        _initValues = {
          'notificationId': _editedNotification.notificationId,
          'ownerId': _editedNotification.ownerId,
          'notificationMessage': _editedNotification.notificationMessage,
          'lastUpdate': _editedNotification.lastUpdate,
        };

        _notificationMessageController.text =
            _initValues['notificationMessage'];
      } else {
        print('notification id = null');
      }
    }

    _isInit = false;
    super.didChangeDependencies();
  }

  void _saveForm() async {
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    if (_editedNotification.notificationId != null) {
      await Provider.of<NotificationProvider>(context, listen: false)
          .updateNotification(
        _editedNotification.notificationId,
        NotificationMessage(
          notificationId: _editedNotification.notificationId,
          ownerId: _editedNotification.ownerId,
          notificationMessage: _editedNotification.notificationMessage,
          lastUpdate: null,
        ),
      );

      Navigator.of(context).pop('Ilmoitus päivitetty onnistuneesti.');
    } else if (_editedNotification.notificationId == null &&
        _editedNotification.notificationMessage != "") {
      await Provider.of<NotificationProvider>(context, listen: false)
          .addNotification(
        NotificationMessage(
          notificationId: _editedNotification.notificationId,
          ownerId: _editedNotification.ownerId,
          notificationMessage: _editedNotification.notificationMessage,
          lastUpdate: null,
        ),
      );

      // closes edit screen, returns to schedule
      Navigator.of(context).pop('Uusi ilmoitus lisätty.');
    } else {
      print('...');
      Navigator.of(context).pop('Uuden ilmoituksen lisäys keskeytetty.');
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void dispose() {
    _notificationMessageController.dispose();
    _notificationMessageFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    //FirebaseAnalytics().logEvent(name: 'Edit_Notification_Screen', parameters: null);
    //final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: AppColors.bg,
      appBar: AppBar(
        title: Text('Muokkaa ilmoitusta'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Form(
          key: _form,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                // notification message field
                Container(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.multiline,
                          controller: _notificationMessageController,
                          focusNode: _notificationMessageFocusNode,
                          minLines: 1,
                          maxLines: 50,
                          style: TextStyles.formFieldLabel,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.textFieldBg,
                            labelText: 'Ilmoituksen viesti',
                            prefixIcon: Icon(
                              Icons.message,
                              color: AppColors.formFieldIcon,
                            ),
                            contentPadding: const EdgeInsets.all(5),
                            border:
                                OutlineInputBorder(borderSide: BorderSide()),
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.lightGreen)),
                          ),
                          textInputAction: TextInputAction.done,
                          onSaved: (newMessage) {
                            _editedNotification.notificationMessage =
                                newMessage;
                          },
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          _notificationMessageController.clear();
                        },
                        color: AppColors.deleteButtonIcon,
                      )
                    ],
                  ),
                ),

                // cancel and save buttons
                if (_isLoading)
                  Container(
                    height: 30,
                    child: Center(
                        child: SizedBox(
                            height: 26,
                            width: 26,
                            child: CircularProgressIndicator())),
                  )
                else
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RaisedButton.icon(
                          color: AppColors.cancelButton,
                          elevation: 3,
                          icon: Icon(
                            Icons.exit_to_app,
                            color: AppColors.cancelButtonIcon,
                          ),
                          onPressed: () {
                            if (_editedNotification.notificationId != null) {
                              Navigator.of(context)
                                  .pop('Muutokset ilmoitukselle hylätty');
                            } else {
                              Navigator.of(context)
                                  .pop('Uuden ilmoituksen lisäys keskeytetty.');
                            }
                          },
                          label: Text(
                            'Peruuta',
                            style: TextStyles.cancelButtonText,
                          )),
                      RaisedButton.icon(
                        elevation: 3,
                        icon: Icon(
                          Icons.save,
                          color: AppColors.saveButtonIcon,
                        ),
                        label:
                            Text('Tallenna', style: TextStyles.saveButtonLabel),
                        color: AppColors.saveButton,
                        onPressed: () async {
                          _saveForm();
                        },
                      ),
                    ],
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
