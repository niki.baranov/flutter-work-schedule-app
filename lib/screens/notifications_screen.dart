// shows all notifications
// has a plus button to add a notification
// each notification is a card with edit & delete buttons/icons

import 'package:flutter/material.dart';
import '../main.dart';
import 'package:provider/provider.dart';

import '../providers/notifications.dart';
import '../screens/edit_notification_screen.dart';
import '../screens/splash_screen.dart';
import '../widgets/notification_item.dart';

class NotificationsScreen extends StatelessWidget {
  static const routeName = '/notifications';

  Future<void> _refreshNotifications(BuildContext context) async {
    await Provider.of<NotificationProvider>(context, listen: false)
        .fetchNotificationsFromLocalDb();
  }

  Future<void> _refreshNotificationsFromServer(BuildContext context) async {
    await Provider.of<NotificationProvider>(context, listen: false)
        .fetchNotificationsFromServer();
  }

  @override
  Widget build(BuildContext context) {
    //FirebaseAnalytics().logEvent(name: 'Notifications_Screen', parameters: null);
    final scaffold = Scaffold.of(context);

    print('** NOTIFICATION SCREEN ** :: building notifications list');

    return Scaffold(
      backgroundColor: Colors.blueGrey[100],
      floatingActionButton: FlatButton.icon(
        icon: Icon(Icons.plus_one),
        label: Text('Lisää ilmoitus'),
        color: AppColors.arrivedAtButton,
        // onPressed: () {
        //   Navigator.of(context).pushNamed(EditNotification.routeName);
        // },
        onPressed: () async {
          {
            final editResult = await Navigator.of(context)
                .pushNamed(EditNotification.routeName);
            scaffold
              ..removeCurrentSnackBar()
              ..showSnackBar(SnackBar(
                  content: Text(
                      editResult != null ? '$editResult' : 'Ei muutoksia')));
          }
        },
      ),
      body: FutureBuilder(
        future: _refreshNotifications(context),
        builder: (ctx, snapshot) => snapshot.connectionState ==
                ConnectionState.waiting
            ? SplashScreen()
            : RefreshIndicator(
                onRefresh: () => _refreshNotificationsFromServer(context),
                child: Consumer<NotificationProvider>(
                  builder: (ctx, notificationData, child) => ListView.builder(
                    itemCount: notificationData.notifications.length,
                    itemBuilder: (ctx, index) {
                      if (notificationData.notifications.length >= 1) {
                        return NotificationItem(
                            notificationData.notifications[index]);
                      } else {
                        return Container();
                      }
                    },
                  ),
                ),
              ),
      ),
    );
  }
}
