// shows a list of days for the logged in users
// time period is set by the user
// dafault time period is current month
// paints each row based on the location
// colors from web version
// tapping on each day opens edit_schedule_screen

import 'dart:core';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:provider/provider.dart';

import '../screens/splash_screen.dart';
import '../providers/schedule.dart';
import '../widgets/schedule_list.dart';

class ScheduleScreen extends StatelessWidget {
  static const routeName = '/schedule';
  final _startDate;
  final _endDate;
  ScheduleScreen(this._startDate, this._endDate);

  Future<void> _refreshLocalSchedule(BuildContext context) async {
    await Provider.of<Schedule>(context, listen: false).fetchLocalSchedule();

    print(
        '** SCHEDULE SCREEN ** :: schedule from local db: ${Provider.of<Schedule>(context, listen: false).items.length}');
  }

  Future<void> _refreshSchedule(BuildContext context) async {
    await Provider.of<Schedule>(context, listen: false).fetchServerSchedule();
  }

  @override
  Widget build(BuildContext context) {
    //FirebaseAnalytics().logEvent(name: 'Schedule_Screen', parameters: null);
    // get from local db first
    return FutureBuilder(
      future: _refreshLocalSchedule(context),
      initialData: Provider.of<Schedule>(context, listen: false).items,
      //future: Provider.of<Schedule>(context, listen: false).fetchLocalSchedule(),
      builder: (ctx, snapshot) =>
          snapshot.connectionState == ConnectionState.waiting
              ? SplashScreen()
              : RefreshIndicator(
                  onRefresh: () => _refreshSchedule(context),
                  child: Consumer<Schedule>(
                    builder: (ctx, scheduleData, child) => ListView.builder(
                      itemCount: 1,
                      itemBuilder: (ctx, index) {
                        var schedule =
                            scheduleData.findByDateRange(_startDate, _endDate);

                        if (schedule.length >= 1) {
                          return ScheduleList(schedule);
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ),
                ),
    );
  }
}
