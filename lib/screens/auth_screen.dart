// handles authentication of a user

import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import '../models/http_exception.dart';
import 'package:provider/provider.dart';
import '../providers/auth.dart';

class AuthScreen extends StatelessWidget {
  static const routeName = '/auth';

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: 'Login_Screen', parameters: null);
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.blueGrey[600]),
          ),
          SingleChildScrollView(
            child: Container(
              height: deviceSize.height,
              width: deviceSize.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                      child:
                          Image(image: AssetImage('assets/logo/taj_icon.png'))),
                  Flexible(
                    flex: deviceSize.width > 600 ? 2 : 1,
                    child: AuthCard(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard({
    Key key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  Map<String, String> _authData = {
    'email': '',
    'password': '',
  };

  var _isLoading = false;

// shows possible errors as a dialog
  void _showErrorDialog(String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('Kirjautumisvirhe!'),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return;
    }
    // start login spinner
    _formKey.currentState.save();

    if (!mounted) return;
    setState(() {
      _isLoading = true;
    });

    // try loop to handle errors
    try {
      // check that address is reachable
      final result = await InternetAddress.lookup('funtor.fi');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        print(result);
      }
      try {
        await Provider.of<Auth>(context, listen: false).login(
          _authData['email'],
          _authData['password'],
        );
      } on HttpException catch (error) {
        print(error);
        print(error.errorCode);
        print(error.message);
        var errorMessage = 'Kirjautuminen epäonnistui!';
        if (error.errorCode == 'incorrect_password') {
          errorMessage = error.message;
        }
        if (error.errorCode == 'incorrect_password') {
          errorMessage = error.message;
        }
        if (error.errorCode == 'invalid_username') {
          errorMessage = error.message;
        }
        if (error.errorCode == 'empty_username') {
          errorMessage = error.message;
        }
        if (error.errorCode == 'empty_password') {
          errorMessage = error.message;
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        const errorMessage =
            'Kirjautuminen epäonnistui, tarkista käyttäjätunnus/salasana ja yritä uudelleen.';
        _showErrorDialog(errorMessage);
      }
    } on SocketException catch (error) {
      print(error);
      var errorMessage = 'Yhteyttä ei voitu muodostaa';
      _showErrorDialog(errorMessage);
    }
    // disable login spinner
    if (!mounted) return;
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: Container(
        width: deviceSize.width * 0.75,
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'Käyttäjänimi'),
                  keyboardType: TextInputType.emailAddress,
                  // add controller for email when its actually used
                  initialValue: '',
                  onSaved: (value) {
                    _authData['email'] = value;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Salasana'),
                  obscureText: true,
                  // add controller for password when its actually used
                  //controller: _passwordController,
                  initialValue: '',
                  onSaved: (value) {
                    _authData['password'] = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                if (_isLoading)
                  Container(
                    padding: const EdgeInsets.all(5),
                    child: Center(
                        child: SizedBox(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator())),
                  )
                else
                  RaisedButton(
                    child: Text('KIRJAUDU'),
                    onPressed: _submit,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 30.0, vertical: 8.0),
                    color: Theme.of(context).primaryColor,
                    textColor: Theme.of(context).primaryTextTheme.button.color,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
