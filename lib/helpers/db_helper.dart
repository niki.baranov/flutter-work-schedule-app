import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DBHelper {
  // Future<void> main() async {
  //   await sql.deleteDatabase(await sql.getDatabasesPath());
  // }

// main scheduleDb
  // function to create and access schedule db
  static Future<Database> database() async {
    // get db path from device
    final dbPath = await sql.getDatabasesPath();

    //await sql.deleteDatabase(dbPath);

    // init db if does not exist
    return sql.openDatabase(path.join(dbPath, 'schedule.db'),
        onCreate: (db, version) async {
      //here we execute a query to drop the table if exists which is called "tableName"
      //and could be given as method's input parameter too
      //db.execute("DELETE * FROM schedule");
      //db.execute("DROP TABLE IF EXISTS schedule");
      await db.execute(
          'CREATE TABLE scheduleList(dayId TEXT PRIMARY KEY, userId TEXT, date TEXT, location TEXT, estimate TEXT, arrivedAt TEXT, leftAt TEXT, unavailable TEXT, other TEXT, message TEXT, updatedAt TEXT)');
      await db.execute(
          'CREATE TABLE userList(userId TEXT PRIMARY KEY, userName TEXT, tableId TEXT)');
      await db.execute(
          'CREATE TABLE fiveDayList(dayId TEXT PRIMARY KEY, userId TEXT, date TEXT, location TEXT, estimate TEXT, arrivedAt TEXT, leftAt TEXT, unavailable TEXT, other TEXT, message TEXT, updatedAt TEXT)');
      await db.execute(
          'CREATE TABLE notificationsList(notificationId TEXT PRIMARY KEY, ownerId TEXT, notificationMessage TEXT, lastUpdate TEXT)');
    }, version: 1);
  }

  // insert data to db
  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DBHelper.database();
    db.insert(table, data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
  }

  // update existing rows with new data, will not change not provided data
  // based on column name and its value
  static Future<void> update(String table, Map<String, Object> data,
      String columnName, String columnValue) async {
    final db = await DBHelper.database();
    db.update(table, data, where: "$columnName = ?", whereArgs: [columnValue]);
  }

  // fetch data from db
  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DBHelper.database();
    return db.query(table);
  }

  // delete a row based on column name and value
  static Future<void> delete(
      String table, String columnName, String columnValue) async {
    final db = await DBHelper.database();
    db.delete(table, where: "$columnName = ?", whereArgs: [columnValue]);
  }
// user db
//   // function to create and access user db
//   static Future<Database> userDatabase() async {
//     // get db path from device
//     final dbPath = await sql.getDatabasesPath();
//     // init db if does not exist
//     return sql.openDatabase(path.join(dbPath, 'userlist.db'),
//         onCreate: (db, version) {
//       return db.execute(
//           'CREATE TABLE userList(userId TEXT PRIMARY KEY, userName TEXT, tableId TEXT)');
//     }, version: 1);
//   }

//   // function to insert users data to db
//   static Future<void> addUsers(String table, Map<String, Object> data) async {
//     //final db = await DBHelper.userDatabase();
//     final db = await DBHelper.database();
//     db.insert(table, data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
//   }

//   // fetch user data from db
//   static Future<List<Map<String, dynamic>>> getUsers(String table) async {
//     //final db = await DBHelper.userDatabase();
//     final db = await DBHelper.database();
//     return db.query(table);
//   }

// // FiveDay
//   // function to create and access schedule db
//   static Future<Database> fiveDay() async {
//     // get db path from device
//     final dbPath = await sql.getDatabasesPath();
//     // init db if does not exist
//     return sql.openDatabase(path.join(dbPath, 'fiveDayList.db'),
//         onCreate: (db, version) {
//       return db.execute(
//           'CREATE TABLE fiveDayList(dayId TEXT PRIMARY KEY, userId TEXT, date TEXT, location TEXT, estimate TEXT, arrivedAt TEXT, leftAt TEXT, unavailable TEXT, other TEXT, message TEXT, updatedAt TEXT)');
//     }, version: 1);
//   }

//   // function to insert data to db
//   static Future<void> addFiveDay(String table, Map<String, Object> data) async {
//     //final db = await DBHelper.fiveDay();
//     final db = await DBHelper.database();
//     db.insert(table, data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
//   }

//   // fetche schedule from db
//   static Future<List<Map<String, dynamic>>> getFiveDay(String table) async {
//     //final db = await DBHelper.fiveDay();
//     final db = await DBHelper.database();
//     return db.query(table);
//   }
}
