class HttpException implements Exception {
  final String errorCode;
  final String message;

  HttpException(this.errorCode, this.message);

  @override
  String toString() {
    return message;
    // return super.toString(); // Instance of HttpException
  }

  // String get errorCode {
  //   return error_code;
  // }

  // String showMessage() {
  //   return message;
  // }
}
