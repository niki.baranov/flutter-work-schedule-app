import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import './providers/holidays.dart';

import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';

import './providers/notifications.dart';
import './providers/server_connection.dart';
import './screens/edit_notification_screen.dart';
import './screens/notifications_screen.dart';
import './providers/schedule.dart';
import './providers/five_day.dart';
import './providers/user.dart';
import './providers/auth.dart';
import './screens/auth_screen.dart';
import './screens/schedule_screen.dart';
import './screens/splash_screen.dart';
import './screens/tabs_screen.dart';
import './screens/edit_schedule_screen.dart';
import './screens/five_day_schedule_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics _analytics = FirebaseAnalytics();
    FirebaseAnalytics().logEvent(name: 'App_Open', parameters: null);
    Intl.defaultLocale = 'en';
    //Sqflite.devSetDebugModeOn(true);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: ServerConnection(),
        ),
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
        // ChangeNotifierProvider(
        //   create: (context) => Schedule(),
        // ),
        ChangeNotifierProxyProvider<Auth, UserProvider>(
          update: (ctx, auth, previousUsers) => UserProvider(
            auth.token,
            auth.userId,
            previousUsers == null ? [] : previousUsers.users,
          ),
          create: (BuildContext ctx) => UserProvider('', null, []),
        ),
        ChangeNotifierProxyProvider<Auth, Holidays>(
          update: (ctx, auth, previousHolidays) => Holidays(
            auth.token,
            auth.userId,
          ),
          create: (BuildContext ctx) => Holidays('', null),
        ),
        ChangeNotifierProxyProvider<Auth, NotificationProvider>(
          update: (ctx, auth, previousNotifications) => NotificationProvider(
            auth.token,
            auth.userId,
            previousNotifications == null
                ? []
                : previousNotifications.notifications,
          ),
          create: (BuildContext ctx) => NotificationProvider('', null, []),
        ),
        ChangeNotifierProxyProvider<Auth, Schedule>(
          update: (ctx, auth, previousSchedule) => Schedule(
            auth.token,
            auth.userId,
            auth.tableId,
            //previousSchedule.findByUserId(auth.userId),
            previousSchedule == null ? [] : previousSchedule.items,
            //users == null ? [] : users.users
          ),
          create: (BuildContext ctx) => Schedule('', null, null, []),
        ),
        ChangeNotifierProxyProvider2<Auth, UserProvider, FiveDay>(
          update: (ctx, auth, users, previousSchedule) => FiveDay(
              auth.token,
              previousSchedule == null ? [] : previousSchedule.fiveday,
              users == null ? [] : users.users),
          create: (BuildContext ctx) => FiveDay('', [], []),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
          locale: Locale('fi'),
          // setup finnish localization
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: const <Locale>[
            // put all locales you want to support here
            const Locale('fi', 'FI'),
            const Locale('en', 'US'),
          ],
          // supportedLocales: [
          //   const Locale('en', 'US'),
          //   const Locale('fi'),
          // ],
          title: 'Työaikajärjestelmä',
          theme: ThemeData(
            primarySwatch: Colors.blueGrey,
            primaryColor: Colors.blueGrey[900],
            accentColor: Colors.orange[900],
            buttonColor: Colors.green,
            fontFamily: 'Lato',
          ),

          // check if user is already logged in
          home: auth.isAuth
              ? TabsScreen()
              : FutureBuilder(
                  future: auth.tryAutoLogin().timeout(Duration(seconds: 2)),
                  builder: (ctx, authResultSnapshot) =>
                      authResultSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : AuthScreen(),
                ),
          routes: {
            ScheduleScreen.routeName: (ctx) => ScheduleScreen(null, null),
            SplashScreen.routeName: (ctx) => SplashScreen(),
            NotificationsScreen.routeName: (ctx) => NotificationsScreen(),
            EditSchedule.routeName: (ctx) => EditSchedule(),
            EditNotification.routeName: (ctx) => EditNotification(),
            FiveDayScheduleScreen.routeName: (ctx) => FiveDayScheduleScreen(),
          },
          navigatorObservers: [
            FirebaseAnalyticsObserver(analytics: _analytics),
          ],
          // onUnknownRoute: (settings) {
          //   return MaterialPageRoute(
          //     builder: (ctx) => ScheduleScreen(_schedule),
          //   );
          // },
        ),
      ),
    );
  }
}

// custom text styles
class FontSizes {
  static double scale = 1;
  static double get body => 14 * scale;
  static double get formFieldLabel => 14 * scale;
  static double get saveButtonLabel => 24 * scale;
  static double get cancelButtonLabel => 22 * scale;
  static double get notificationText => 16 * scale;
}

class AppColors {
  static Color get arrivedAtButton => Color.fromRGBO(119, 255, 122, 1);
  static Color get leftAtButton => Color.fromRGBO(244, 67, 54, 1);
  static Color get saveButton => Color.fromRGBO(78, 172, 85, 1);
  static Color get saveButtonIcon => Colors.green[100];
  static Color get deleteButtonIcon => Color.fromRGBO(244, 67, 54, 1);
  static Color get cancelButton => Color.fromRGBO(120, 120, 120, 1);
  static Color get cancelButtonIcon => Colors.black45;
  static Color get estimate => Color.fromRGBO(247, 155, 13, 1);
  static Color get arrivedAt => Color.fromRGBO(78, 172, 85, 1);
  static Color get leftAt => Color.fromRGBO(244, 67, 54, 1);
  static Color get bg => Colors.blueGrey[100];
  static Color get textFieldBg => Color.fromRGBO(255, 255, 255, 1);
  static Color get dayInfo => Color.fromRGBO(0, 0, 0, 1);
  static Color get formFieldLabel => Color.fromRGBO(120, 120, 120, 1);
  static Color get formTime => Color.fromRGBO(255, 255, 255, 1);
  static Color get formFieldIcon => Colors.black45;
  static Color get dayHoliday => Color.fromRGBO(244, 67, 54, 1);
  static Color get drawerLabelGrey => Color.fromRGBO(120, 120, 120, 1);
  static Color get weekMonthSeparator => Colors.blueGrey[500];
  static Color get disabledIconButton => Colors.blueGrey[100];
  static Color get enabledIconButton => Colors.green[400];
}

class TextStyles {
  static TextStyle get formFieldLabel => TextStyle(
      fontSize: FontSizes.formFieldLabel, color: AppColors.formFieldLabel);
  static TextStyle get formTime => TextStyle(
      fontSize: FontSizes.formFieldLabel,
      color: AppColors.formTime,
      fontWeight: FontWeight.bold);
  static TextStyle get drawerLabel =>
      TextStyle(fontSize: FontSizes.body, color: Colors.black);
  static TextStyle get drawerLabelGrey =>
      TextStyle(fontSize: FontSizes.body, color: AppColors.drawerLabelGrey);
  static TextStyle get cancelButtonText =>
      TextStyle(fontSize: FontSizes.cancelButtonLabel, color: Colors.black45);
  static TextStyle get saveButtonLabel =>
      TextStyle(fontSize: FontSizes.saveButtonLabel, color: Colors.green[100]);
  static TextStyle get notificationText =>
      TextStyle(fontSize: FontSizes.notificationText, color: Colors.black);
  static TextStyle get weekMonthSeparator =>
      TextStyle(fontSize: FontSizes.body, color: Colors.grey[100]);
  static TextStyle get dayLocation => TextStyle(
      fontSize: FontSizes.body,
      color: Colors.black,
      fontWeight: FontWeight.bold);
  static TextStyle get dateRangeSelector => TextStyle(
      fontSize: FontSizes.body,
      color: Colors.white,
      fontWeight: FontWeight.bold);
}
