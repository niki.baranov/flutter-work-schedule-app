library constants;

// server
const String _api = 'http://your-wordpress-site.com/wp-json';

const String getAuthUrl = '$_api/jwt-auth/v1/token';

// fetch 5 day schedule for all users
const String getFiveDayUrl = '$_api/schedule/v1/fiveday/';

// fetch all users
const String getUsers = '$_api/ninjausers/v1/all';

// fetch total and available holidays
const String getHolidays = '$_api/holidays/v1/all/';

// notification management
const String getNotificationsUrl = '$_api/notifications/v1/all';
const String updateNotificationUrl = '$_api/notifications/v1/update/';
const String createNotificationUrl = '$_api/notifications/v1/create/';
const String deleteNotificationUrl = '$_api/notifications/v1/delete/';

// personal schedule management
const String getScheduleUrl = '$_api/schedule/v1/all/';
const String updateScheduleUrl = '$_api/update_schedule/v1/update/';
const String updateScheduleTimeUrl = '$_api/update_schedule/v1/update_time/';
